package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.Home_TIM;
import paginas.Log;

public class modRecarga_Infinity {
	////////////////////////////////////////////////////////////
	/////////////// Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;
	Util Ut;
	long pageLoadTime_Seconds;

	Home_TIM HomeTIM;
	
	public void Teste_Home(WebDriver driver) throws IOException, AWTException, InterruptedException {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////
		Ut = new Util(driver);
		Date myDateIni = null;	
		StopWatch pageLoad = new StopWatch();
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");

		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////	

		try {
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada
			///////////////////////////////////////////Data modifica��o 17/04/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			//Abre Site
			Log.info("Site TIM - Inicio Jornada Home");	

			Ut.checkPageIsReady(driver,15);
			boolean telaOk = driver.getPageSource().contains("PLANO");

			if (telaOk == true )	{

				// Valida��o Saldo
				HomeTIM = new Home_TIM(driver);
				Ut.TempoLoad_Total();
				Ut.FotoZ("Home");
				Log.info("Selecionando a opcao no menu Plano > Infinity Turbo 7.");
				Ut.MovePara(HomeTIM.menuPlanos, driver);
				HomeTIM.menuInfinityTurbo7.click();
				
				Log.info("Validando se a opcao contrate com cartao de credito  esta presente.");
				Ut.TrocarFrame(HomeTIM.iframeInfinityTurbo);
				if (Ut.EsperaElemento(HomeTIM.labelContrateCartaoCredito) == false || Ut.EsperaElemento(HomeTIM.inputContrateCartaoCredito) == false) {				
					pageLoad.stop();
					//Ut.MovePara(HomePage.lnk_DebAut, driver);
					Ut.FotoZ("Erro_Home_RecargaInfinity");
					Log.error("Erro na Home: Erro ao carregar opcao contrate com cartao de credito.");
					//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site TIM - Jornada Conta Online;Home;Recarga (Infinity Turbo 7);NA;Erro;Erro ao carregar opcao contrate com cartao de credito;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site TIM - Jornada Conta Online;Home;Recarga (Infinity Turbo 7);NA;Erro;Erro ao carregar opcao contrate com cartao de credito;" + (int) Ut.TempoLoad_Total());
					driver.quit();
					Assert.assertTrue("Erro ao carregar opcao contrate com cartao de credito", false);
				}	
				
				Ut.MovePara(HomeTIM.inputContrateCartaoCredito, driver);
				Ut.FotoZ("ContrateCartaoCredito");
				pageLoad.stop();
				
				Log.info("Opcao contrate com cartao de credito carregada.");
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Home;Recarga (Infinity Turbo 7);NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Home;Recarga (Infinity Turbo 7);NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				Log.info("Site TIM - Fim Jornada Home");	
				
			} else {
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.FotoZ("Erro_Home");
					Log.error("Erro na Home: pagina n�o carregada.");
					Log.endTestCase("Site_TIM_HOME_Saldo");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Recarga (Infinity Turbo 7);NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Home;v;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					Assert.assertTrue("Problema Pagina Home", telaOk==true);
				}	
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
		///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
		////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			catch (Exception ee) {
				String ErMgs = "Erro";
				StringWriter sw = new StringWriter();
				ee.printStackTrace(new PrintWriter(sw));
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Home");
				Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
				Log.error(sw.toString());
				Log.endTestCase("Site_TIM_HOME_RecargaExpress");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Recarga (Infinity Turbo 7);NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Home;Recarga (Infinity Turbo 7);NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				//driver.quit();
				Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
				//Comentar antes da vers�o final
				ee.printStackTrace();
			}	
		}
	}