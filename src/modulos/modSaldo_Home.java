package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.Home;
import paginas.Log;

public class modSaldo_Home {
	////////////////////////////////////////////////////////////
	/////////////// Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;
	Util Ut = new Util(driver);
	long pageLoadTime_Seconds;
	Home HomePage;
	
	public void Teste_Home(WebDriver driver) throws IOException, AWTException, InterruptedException {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////
				
		Date myDateIni = null;	
		StopWatch pageLoad = new StopWatch();
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////	

		try {
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada
			///////////////////////////////////////////Data modifica��o 17/04/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			//Abre Site
			Log.info("Site MEUTIM - Inicio Jornada Home");	

			Ut.checkPageIsReady(driver,15);
			//boolean telaOk = driver.getPageSource().contains("PLANO");

			//if (telaOk == true )	{

				// Valida��o Saldo
				HomePage = new Home(driver);
				Ut.TempoLoad_Total();

				if (Ut.EsperaElemento(HomePage.tabela_saldo) == false) {				
					pageLoad.stop();
					//Ut.MovePara(HomePage.lnk_DebAut, driver);
					Ut.FotoZ("Erro_Home_TabelaCreditos");
					Log.error("Erro na Home: Erro Tabela Creditos.");
					//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Credito;NA;Erro;Erro ao carregar Pagina Home;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Credito;NA;Erro;Erro ao carregar Pagina Home;" + (int) Ut.TempoLoad_Total());
					driver.quit();
					Assert.assertTrue("Problema carregar tabela Cr�ditos", Ut.EsperaElemento(HomePage.tabela_consumo_dados));
				}	
				
				if (Ut.EsperaElemento(HomePage.propaganda))
					Ut.clicar(HomePage.propaganda, driver);
				Ut.FotoZ("Home");
				Ut.MovePara(HomePage.tabela_saldo, driver);
				Ut.FotoZ("Home2");
				Log.info("Validando informa��es de Saldo:");
		
				String[] dados = HomePage.tabela_saldo.getText().split("\n");
				
				for (String info : dados){
					if (info.contains("|") || info.equals("")) {
						continue;
					}
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Saldo;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Saldo;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
					Log.info("Texto:" + info);
				}
				if (dados[0].contains("Sistema temporariamente indispon?vel"))
					log("Erro_Home_SecaoConsumoDados","Mensagem de erro: " + dados[0],dt, myDateIni, hr, false);
				if (!dados[0].contains("Saldo atual"))
					log("Erro_Home_Saldo","Informacoes de Saldo vazia.", dt, myDateIni, hr, false);
				if (!dados[1].contains("R$") || dados[1].replace("R$", "").isEmpty())
					log("Erro_Home_Saldo","Informacoes de Saldo vazia.", dt, myDateIni, hr, false);
				if (!dados[2].contains("sujeito a altera��o") || dados[1].replace(" sujeito a altera��o.", "").replace("�s ", "").replace(" dia ", "").isEmpty())
					log("Erro_Home_Saldo","Informacoes de Saldo vazia.", dt, myDateIni, hr, false);
				
				pageLoad.stop();
				
				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Saldo;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Saldo;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				Log.info("Site MEUTIM - Fim Jornada Home");	
			/*} else {
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.FotoZ("Erro_Home");
					Log.error("Erro na Home: pagina n�o carregada.");
					Log.endTestCase("Site_MeuTIM_HOME_Saldo");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Saldo;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Home;Saldo;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					Assert.assertTrue("Problema Saldo", telaOk==true);
				}*/	
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
		///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
		////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			catch (Exception ee) {
				String ErMgs = "Erro";
				StringWriter sw = new StringWriter();
				ee.printStackTrace(new PrintWriter(sw));
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Home");
				Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
				Log.error(sw.toString());
				Log.endTestCase("Site_MeuTIM_HOME_ConsumoDadosVoz");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				//driver.quit();
				Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
				//Comentar antes da vers�o final
				ee.printStackTrace();
			}	
		}
	
		public void log(String nomePrint, String erro, SimpleDateFormat dt, Date myDateIni, SimpleDateFormat hr, boolean result) throws IOException, InterruptedException, AWTException{
			Ut.FotoZ(nomePrint); //"Erro_Home_SecaoConsumoDados"
			Log.error("Erro na Home: " + erro); //"Erro na Home: Informacoes de Franquia Dados vazia."
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Erro;" + erro + ";" + (int) Ut.TempoLoad_Total());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Erro;"  + erro + ";" + (int) Ut.TempoLoad_Total());
			driver.quit();
			Assert.assertTrue(erro, result);
		}
	}