package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.ExtratoPre;
import paginas.Home;
import paginas.Home_TIM;
import paginas.Log;

public class modExtratoPre_DetalheConsumo {
	////////////////////////////////////////////////////////////
	/////////////// Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;
	Util Ut;
	long pageLoadTime_Seconds;

	Home Home;
	ExtratoPre ExtratoPre;
	
	public void Teste_Home(WebDriver driver) throws IOException, AWTException, InterruptedException {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////
		Ut = new Util(driver);
		Date myDateIni = null;	
		StopWatch pageLoad = new StopWatch();
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");

		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////	

		try {
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada
			///////////////////////////////////////////Data modifica��o 17/04/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			//Abre Site
			Log.info("Site Meu TIM - Inicio Jornada Home");	

			Ut.checkPageIsReady(driver,10);
			boolean telaOk = driver.getPageSource().contains("PLANO");

			//if (telaOk == true )	{

				// Valida��o Saldo
				Home = new Home(driver);
				ExtratoPre = new ExtratoPre(driver);
				Ut.TempoLoad_Total();
				
				if (Ut.EsperaElemento(Home.propaganda))
					Ut.clicar(Home.propaganda, driver);
				
				Ut.FotoZ("Home");
				Log.info("Selecionando a opcao Meus Creditos > Consultar detalhamento de consumo");
				//Ut.TrocarFrame(HomeTIM.iframeRecargaExpress);
				Home.menu_meusCreditos.click();
				Ut.FotoZ("MeusCreditos");
				ExtratoPre.buttonConsultarDetalheConsumo.click();
				Ut.FotoZ("ConsultaDetalheConsumo");
				
				Log.info("Selecionando os filtros para a consulta");
				ExtratoPre.dataInicio.sendKeys("05/04/2017");
				ExtratoPre.dataFim.sendKeys("17/04/2017");
				ExtratoPre.checkTodosServicos.click();
				Ut.FotoZ("DetalheConsumoCampos");
				ExtratoPre.buttonGerarRelatorio.click();
				
				Log.info("Verificando se existe alguma transacao efetuada no periodo");
				if (Ut.EsperaElemento(ExtratoPre.msgSemConsumo)){
					Log.info("Nenhuma transacao efetuada no periodo selecionado");
					Ut.MovePara(ExtratoPre.msgSemConsumo, driver);
					Ut.FotoZ("DetalheConsumoMensagem");
				} else {
					Log.info("Foram realizadas transacoes no periodo selecionado");
					if(!Ut.EsperaElemento(ExtratoPre.tabelaDetalhamentoConsumo)){
						
						pageLoad.stop();
						//Ut.MovePara(HomePage.lnk_DebAut, driver);
						Ut.FotoZ("Erro_ExtratoPre");
						Log.error("Erro no Extrato: Erro ao carregar tabela de transacoes.");
						//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
						Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site Meu TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Erro;Erro ao carregar tabela de transacoes.;" + (int) Ut.TempoLoad_Total());
						System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site Meu TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Erro;Erro ao carregar tabela de transacoes.;" + (int) Ut.TempoLoad_Total());
						driver.quit();
						Assert.assertTrue("Erro ao carregar tabela de transacoes.", Ut.EsperaElemento(ExtratoPre.tabelaDetalhamentoConsumo));
					}
					Ut.MovePara(ExtratoPre.tabelaDetalhamentoConsumo, driver);
					Ut.FotoZ("DetalheConsumoTabela");
				}

				Log.info("Consulta de detalhamento de consumo realizada");
				pageLoad.stop();
				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site Meu TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site Meu TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				Log.info("Site Meu TIM - Fim Jornada Home");	
				
				
			/*} else {
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.FotoZ("Erro_Home");
					Log.error("Erro na Home: pagina n�o carregada.");
					Log.endTestCase("Site_TIM_HOME_Saldo");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Extrato Pr�;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					Assert.assertTrue("Problema Pagina Home", telaOk==true);
				}*/	
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
		///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
		////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			catch (Exception ee) {
				String ErMgs = "Erro";
				StringWriter sw = new StringWriter();
				ee.printStackTrace(new PrintWriter(sw));
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Home");
				Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
				Log.error(sw.toString());
				Log.endTestCase("Site_MeuTIM_HOME_RecargaExpress");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site Meu TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site Meu TIM - Jornada Conta Online;Extrato;Extrato Pr�;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				//driver.quit();
				Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
				//Comentar antes da vers�o final
				ee.printStackTrace();
			}	
		}
	}