package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.DownloadPdf;
import paginas.Home;
import paginas.Log;

public class modValidaFatura_Home {
	
	Home HomePage;
	DownloadPdf Download;
	

public void Teste_FuturaHome(WebDriver driver) throws IOException, AWTException, InterruptedException {
	
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis///////////////////////////
	Util Ut = new Util(driver);
	Date myDateIni = null;
	StopWatch pageLoad = new StopWatch();
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat dt_pdf = new SimpleDateFormat("dd/MM/yy");
	String ArquivoPDF;
	String dirDest = "C:/SitesTIM_Robo_ContaOnline/Downloads/";	

    long pageLoadTime_Seconds;
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis///////////////////////////


	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
	///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
	////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////	

	try {	
		
	myDateIni = new Date();
	
	//Validacao Fatura
	Log.info("Site MEUTIM - Inicio Jornada Valida PDF");	
	Log.info("Seleciona atalho para download da ultima fatura do cliente");
	//pageLoad.reset();
	pageLoad.start();
	HomePage = new Home(driver);
	Ut.ValidarElementosNoHomeTabela(HomePage.tr_collection);
	Log.info("Seleciona janela de download do PDF");
	Ut.troca_aba(1, driver);
	Ut.checkPageIsReady(driver,60);
	Log.info("Janela Download PDF Carregada");
	String Tela = driver.getPageSource();
	boolean Tx = Tela.contains("Seu arquivo est� pronto para Download!");
	boolean Tx2 = Tela.contains("Confirma a gera��o do arquivo?");
	boolean Tx3 = Tela.contains("302 Moved");
	
	//Seu arquivo est� pronto para Download!				
	if (Tx3 == true) {
		pageLoad.stop();
		pageLoadTime_Seconds = pageLoad.getTime() / 1000;
		Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura - Http 302;" + (int) pageLoadTime_Seconds);
		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura - Http 302;" + (int) pageLoadTime_Seconds);
		Ut.FotoZ("Erro_DownloadPDF");
		Log.error("Erro no DownLoad do PDF");
		Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
		driver.close();
		Ut.troca_aba(0, driver);
		driver.quit();
	} else	if (Tx == false && Tx2 == false) {
		pageLoad.stop();
		pageLoadTime_Seconds = pageLoad.getTime() / 1000;
		Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura;" + (int) pageLoadTime_Seconds);
		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura;" + (int) pageLoadTime_Seconds);
		Ut.FotoZ("Erro_DownloadPDF");
		Log.error("Erro no DownLoad do PDF");
		Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
		driver.quit();
	} 
	
	String telaDown = driver.getPageSource();
	
//	if (telaDown.contains("botao1") == true) { //Tx2 == true
//		Log.info("Confirma��o Download do Arquivo");
//		Download = new DownloadPdf(driver);
//		Download.btn_Sim.click();
//		//Thread.sleep(5000);
//	} 	
	
	if (telaDown.contains("botao1") == true) { //Tx2 == true
		Log.info("Confirma��o Download do Arquivo");
		Ut.FotoZ("DownloadPDF");
		Download = new DownloadPdf(driver);
		Download.btn_Sim.click();
		} else {
		Log.info("Download Autom�tico");		
	}
	
	Log.info("Foto Tela Download PDF Carregada");
	
	//Valida ser arquivo esta no diret�rio
	Log.info("Espera finaliza��o do download do PDF Carregada");
	Ut.checkArqReady(driver, 500, dirDest, ".pdf");
	ArquivoPDF = Ut.NomeArq_Ext(dirDest, ".pdf");
	Log.info("Nome do Arquivo: " + ArquivoPDF);
	Log.info("Foto Tela Download PDF Carregada");
	Ut.FotoZ("DownloadPDF");
	Log.info("Fecha Tela Download PDF Carregada");
	driver.close();
	Ut.troca_aba(0, driver);
	
	String DVencimento = dt_pdf.format(dt_pdf.parse((HomePage.DtVencimento.getText())));
	String Valor = HomePage.Valor.getText();
		
	//Validar valores
	boolean vVencimento = Ut.PegaPdf(DVencimento,dirDest+ArquivoPDF);
	boolean vValor = Ut.PegaPdf(Valor,dirDest+ArquivoPDF);

	pageLoad.stop();
	pageLoadTime_Seconds = pageLoad.getTime() / 1000;
	
	//Abre arquivo PDF
	Ut.abrePdf(dirDest + Ut.NomeArq_Ext(dirDest, ".pdf"));  
	Ut.FotoZ("FaturaPDF");	
	Ut.fechaTela();
	
if (vVencimento == true || vValor == true) {
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
	System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
} else if (vVencimento == true) {				
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
} else if (vValor == true) {				
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds/2);
}

if (vVencimento == false) {				
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
} 

if (vValor == false) {	
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
	+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
} 
	Log.info("Site MEUTIM - Fim Jornada Valida PDF");
	
}	
///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

catch (Exception ee) {
	String ErMgs = "Erro";
	StringWriter sw = new StringWriter();
	ee.printStackTrace(new PrintWriter(sw));
	// Foto Tela Login erro
	Ut.FotoZ("Erro_Home");
	Log.error("Erro ao Validar Fatura: Erro Gen�rico." + ee.getCause());
	Log.error(sw.toString());
	//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	pageLoad.stop();
	pageLoadTime_Seconds = pageLoad.getTime() / 1000;
	Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;Download PDF;Erro;Erro ao Validar Fatura: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
	System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;Download PDF;Erro;Erro ao Validar Fatura: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
	Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
}			

}
}



