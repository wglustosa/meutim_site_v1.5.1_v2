package modulos;

import org.testng.AssertJUnit;

import config.Util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.WebDriver;

import paginas.Log;
import paginas.Login;
import paginas.Login_Error;

public class modLogin {
	WebDriver driver;
	String ErMgs;
	// Construtor
	// public modLogin(WebDriver driver){
	// modLogin.driver = driver;
	// }

	public void Login(WebDriver driver) throws Exception {
		
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis///////////////////////////
		Date myDateIni = null;
		Util Ut = new Util(driver);
		Login LoginPage;
		Login_Error LoginErrorPage;
		StopWatch pageLoad = new StopWatch();
		boolean Er;
		boolean Er2;
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		String msgEr;
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis///////////////////////////
		
		try {

			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
			///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			myDateIni = new Date();
			// Layout Saida
			System.out.println("Data execu��o;Hora;KPI;Tela;Nome Card;Elemento;Carregou;Motivo;Tempo Execu��o");

			// Abre Site
			Log.info("Site MEUTIM - Inicio Jornada Login");
			
			
			pageLoad.start();
			driver.get("https://meutim.tim.com.br/");
			Ut.checkPageIsReady(driver, 10);
			pageLoad.stop();
			
			boolean telaOk = driver.getPageSource().contains("Escolha o perfil");

			if (telaOk == true) {
				
				Log.info("Site Conta Online;Pagina MeuTIM;NA;Sucesso;");
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Pagina MeuTIM;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Pagina MeuTIM;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());			
				
				// Realizada Login Site MeuTIM
				Ut.FotoZ("Login");
				LoginPage = new Login(driver);
				pageLoad.reset();
				pageLoad.start();
				LoginPage.LogIn_Action("21981178990", "8457");
				Ut.TempoLoad_Parcial();

				pageLoad.stop();
				pageLoad.reset();
				pageLoad.start();

				Er = LoginPage.Login_Error_ForaAr();
				Er2 = LoginPage.Login_Invalido();
				
				// Compara URL pra saber se realmente trocouo de pagina
				Ut.TrocaPagina(driver.getCurrentUrl(), driver, 15);

				// Gera output
				if (Er == false && Er2 == false) {
					Log.info("Login Aceito");
					Log.info("Site Conta Online;Login;NA;Sucesso;");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());
				} else {
					// Pega valor em caso de erro
					LoginErrorPage = new Login_Error(driver);
					msgEr = LoginErrorPage.msg_Erro.getText();
					Log.info("Valida Erro no Login");
					// Foto Tela Login erro
					Ut.FotoZ("Erro_Login");
					Log.error("Erro no Login: " + msgEr);
					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					// Falha Teste em Caso de retorno false

					if (Er == true) {
						Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Site Fora do Ar;"
								+ Ut.TempoLoad_Parcial());
						System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Site Fora do Ar;"
								+ Ut.TempoLoad_Parcial());
					} 
					if (Er2 == true) {
						Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Login ou Senha inv�lidos;"
								+ Ut.TempoLoad_Parcial());
						System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Login ou Senha inv�lidos;"
								+ Ut.TempoLoad_Parcial());
					}
					AssertJUnit.assertFalse("Sistema Fora do Ar",
							driver.getPageSource().contains("Desculpe, o sistema est�"));
					AssertJUnit.assertFalse("Usu�rio ou senha inv�lida",
							driver.getPageSource().contains("Usu�rio ou senha inv�lida"));
					driver.quit();
				}
				Log.info("Site MEUTIM - Fim Jornada Login");
			} else {
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Login");
				Log.error("Erro no Login: Problema na rede - pagina n�o carregada.");
				Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;"
						+ Ut.TempoLoad_Parcial());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;"
						+ Ut.TempoLoad_Parcial());
				driver.quit();
				AssertJUnit.assertTrue("Problema na Rede", telaOk);
			}

			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo - Thiago Cruz
			/////////////////////////////////////////////////////////////////////////////////////////////////////////// 17/03/2017///////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////

		} 
		///////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////////Bloco do Tratamento de erros n�o mapeados///////////////////
		catch (Exception ee) {
			// Foto Tela Login erro
			ErMgs = "TimeOut";
			StringWriter sw = new StringWriter();
			ee.printStackTrace(new PrintWriter(sw));
			Ut.FotoZ("Erro_Login");
			Log.error("Erro no Login: Erro Gen�rico." + ee.getCause());
			Log.error(sw.toString());
			//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Erro no Login: Erro Gen�rico.;"
					+ Ut.TempoLoad_Parcial());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Erro no Login: Erro Gen�rico.;"
					+ Ut.TempoLoad_Parcial());
					// Comentar na vers�o final
			ee.printStackTrace();
			AssertJUnit.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
		}
	}
	
	public void Login(WebDriver driver, String login, String senha, String url) throws Exception {
		
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis///////////////////////////
		Date myDateIni = null;
		Util Ut = new Util(driver);
		Login LoginPage;
		Login_Error LoginErrorPage;
		StopWatch pageLoad = new StopWatch();
		boolean Er;
		boolean Er2;
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		String msgEr;
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis///////////////////////////
		
		try {

			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
			///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			myDateIni = new Date();
			// Layout Saida
			System.out.println("Data execu��o;Hora;KPI;Tela;Nome Card;Elemento;Carregou;Motivo;Tempo Execu��o");

			// Abre Site
			Log.info("Site MEUTIM - Inicio Jornada Login");
			
			
			pageLoad.start();
			driver.get(url);
			Ut.checkPageIsReady(driver, 10);
			pageLoad.stop();
			
			boolean telaOk = driver.getPageSource().contains("Escolha o perfil");

			if (telaOk == true) {
				
				Log.info("Site Conta Online;Pagina MeuTIM;NA;Sucesso;");
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Pagina MeuTIM;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Pagina MeuTIM;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());			
				
				// Realizada Login Site MeuTIM
				Ut.FotoZ("Login");
				LoginPage = new Login(driver);
				pageLoad.reset();
				pageLoad.start();
				LoginPage.LogIn_Action(login, senha);
				Ut.TempoLoad_Parcial();

				pageLoad.stop();
				pageLoad.reset();
				pageLoad.start();

				Er = LoginPage.Login_Error_ForaAr();
				Er2 = LoginPage.Login_Invalido();

				// Gera output
				if (Er == false && Er2 == false) {
					Log.info("Login Aceito");
					Log.info("Site Conta Online;Login;NA;Sucesso;");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Sucesso;NA;" + Ut.TempoLoad_Parcial());
				} else {
					// Pega valor em caso de erro
					LoginErrorPage = new Login_Error(driver);
					msgEr = LoginErrorPage.msg_Erro.getText();
					Log.info("Valida Erro no Login");
					// Foto Tela Login erro
					Ut.FotoZ("Erro_Login");
					Log.error("Erro no Login: " + msgEr);
					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					// Falha Teste em Caso de retorno false

					if (Er == true) {
						Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Site Fora do Ar;"
								+ Ut.TempoLoad_Parcial());
						System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Site Fora do Ar;"
								+ Ut.TempoLoad_Parcial());
					} 
					if (Er2 == true) {
						Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Login ou Senha inv�lidos;"
								+ Ut.TempoLoad_Parcial());
						System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
								+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Login ou Senha inv�lidos;"
								+ Ut.TempoLoad_Parcial());
					}
					AssertJUnit.assertFalse("Sistema Fora do Ar",
							driver.getPageSource().contains("Desculpe, o sistema est�"));
					AssertJUnit.assertFalse("Usu�rio ou senha inv�lida",
							driver.getPageSource().contains("Usu�rio ou senha inv�lida"));
					driver.quit();
				}
				Log.info("Site MEUTIM - Fim Jornada Login");
			} else {
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Login");
				Log.error("Erro no Login: Problema na rede - pagina n�o carregada.");
				Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;"
						+ Ut.TempoLoad_Parcial());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;"
						+ Ut.TempoLoad_Parcial());
				driver.quit();
				AssertJUnit.assertTrue("Problema na Rede", telaOk);
			}

			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo - Thiago Cruz
			/////////////////////////////////////////////////////////////////////////////////////////////////////////// 17/03/2017///////////////////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////

		} 
		///////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////////Bloco do Tratamento de erros n�o mapeados///////////////////
		catch (Exception ee) {
			// Foto Tela Login erro
			ErMgs = "TimeOut";
			StringWriter sw = new StringWriter();
			ee.printStackTrace(new PrintWriter(sw));
			Ut.FotoZ("Erro_Login");
			Log.error("Erro no Login: Erro Gen�rico." + ee.getCause());
			Log.error(sw.toString());
			//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Erro no Login: Erro Gen�rico.;"
					+ Ut.TempoLoad_Parcial());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Erro no Login: Erro Gen�rico.;"
					+ Ut.TempoLoad_Parcial());
					// Comentar na vers�o final
			ee.printStackTrace();
			AssertJUnit.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
		}
	}
}
