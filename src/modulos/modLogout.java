package modulos;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.Home;
import paginas.Log;

public class modLogout {
	////////////////////////////////////////////////////////////
	///////////////Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;

	// Construtor
	// public modLogout(WebDriver driver){
	// modLogout.driver = driver;
	// }

	public void Logout(WebDriver driver) throws Exception {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////

		Date myDateIni = null;
		Util Ut = new Util(driver);
		Home HomePage;
		StopWatch pageLoad = new StopWatch();
		long pageLoadTime_Seconds;
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////

		
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		try {
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			// Valida��o Logout
			HomePage = new Home(driver);
			Log.info("Site MEUTIM - Inicio Jornada Logout");
			Ut.checkPageIsReady(driver, 10);
			Log.info("Move tela para elemento de logout");
			Ut.MovePara(HomePage.btn_Sair, driver);
			Log.info("Chama a��o de logout");
			HomePage.LogOut_Action();
			Log.info("Espera tela de confirma��o de logout");
			Ut.EsperaElemento(HomePage.btn_ConfirmaSair);
			Log.info("Confirmar logout");
			Ut.FotoZ("Home_Logout");
			if (Ut.EsperaElemento(HomePage.btn_ConfirmaSair))
				HomePage.btn_ConfirmaSair.click();
			Log.info("Logout Realizado com sucesso");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Sucesso;NA;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Sucesso;NA;" + pageLoadTime_Seconds);
			Log.info("Site MEUTIM - Fim Jornada Logout");
			//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
		}
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo - Thiago Cruz
		/////////////////////////////////////////////////////////////////////////////////////////////////////////// 17/03/2017///////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////	
		///////////////////////////////////////////////Bloco do Tratamento de erros n�o mapeados///////////////////
		catch (Exception e) {
			// pageLoad.stop();
			// pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			// Foto Tela Login erro
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			Ut.FotoZ("Erro_Home_logout");
			Log.error("Erro na pagina Home ao realizar logout: Erro gen�rico." + e.getCause());
			Log.error(sw.toString());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro gen�rico;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro gen�rico;" + pageLoadTime_Seconds);
		}
	}
}
