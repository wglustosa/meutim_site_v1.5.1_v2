package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.Home_TIM;
import paginas.Log;
import paginas.MapaCobertura_TIM;

public class modMapaCobertura_Mapa {
	////////////////////////////////////////////////////////////
	/////////////// Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;
	Util Ut;
	long pageLoadTime_Seconds;

	Home_TIM HomeTIM;
	MapaCobertura_TIM MapaCoberturaTIM;
	
	public void Teste_Home(WebDriver driver) throws IOException, AWTException, InterruptedException {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////
		Ut = new Util(driver);
		Date myDateIni = null;	
		StopWatch pageLoad = new StopWatch();
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");

		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////	

		try {
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada
			///////////////////////////////////////////Data modifica��o 17/04/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			//Abre Site
			Log.info("Site TIM - Inicio Jornada Home");	

			Ut.checkPageIsReady(driver,10);
			boolean telaOk = driver.getPageSource().contains("PLANO");

			if (telaOk == true )	{
				// Valida��o Saldo
				HomeTIM = new Home_TIM(driver);
				MapaCoberturaTIM = new MapaCobertura_TIM(driver);
				Ut.TempoLoad_Total();
				Ut.FotoZ("HomeTim");
				Log.info("Acessando a pagina Mapa de Cobertura");
				HomeTIM.menuMapaCobertura.click();
				
				Ut.TrocarFrame(MapaCoberturaTIM.iframeMapaCoberturaParent);
				Ut.TrocarFrame(MapaCoberturaTIM.iframeMapaCobertura);
				Ut.MovePara(MapaCoberturaTIM.inputCEP, driver);
				Log.info("Informando CEP");
				MapaCoberturaTIM.inputCEP.sendKeys("04795100");
				Ut.FotoZ("PesquisaMapaCobertura");
				MapaCoberturaTIM.buttonLupa.click();
				Ut.FotoZ("ResultadoPesquisaMapaCobertura");
				
				if (Ut.EsperaElemento(MapaCoberturaTIM.mapa) == false || Ut.EsperaElemento(MapaCoberturaTIM.buttonMapa) == false || Ut.EsperaElemento(MapaCoberturaTIM.buttonSatelite) == false) {				
					pageLoad.stop();
					//Ut.MovePara(HomePage.lnk_DebAut, driver);
					Ut.FotoZ("Erro_MapaCobertura");
					Log.error("Erro na Pagina Mapa de Cobertura WEB: Erro ao carregar o mapa");
					//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Erro;Erro na Pagina Mapa de Cobertura WEB: Erro ao carregar o mapa;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Erro;Erro na Pagina Mapa de Cobertura WEB: Erro ao carregar o mapa;" + (int) Ut.TempoLoad_Total());
					driver.quit();
					Assert.assertTrue("Erro na Pagina Mapa de Cobertura WEB: Erro ao carregar o mapa", Ut.EsperaElemento(HomeTIM.recargaExpress));
				}	
				
				Log.info("Mapa de cobertura carregado.");
				pageLoad.stop();
				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				Log.info("Site TIM - Fim Jornada Home");	
				
				
			} else {
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.FotoZ("Erro_Home");
					Log.error("Erro na Home: pagina n�o carregada.");
					Log.endTestCase("Site_TIM_MapaCoberturaWEB");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Mapa de cobertura  WEB;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					Assert.assertTrue("Problema Pagina Home", telaOk==true);
				}	
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
		///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
		////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			catch (Exception ee) {
				String ErMgs = "Erro";
				StringWriter sw = new StringWriter();
				ee.printStackTrace(new PrintWriter(sw));
				// Foto Tela Login erro
				Ut.FotoZ("Site_TIM_MapaCoberturaWEB");
				Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
				Log.error(sw.toString());
				Log.endTestCase("Site_TIM_HOME_RecargaExpress");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site TIM - Jornada Conta Online;Mapa de Cobertura;Mapa de cobertura  WEB;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				//driver.quit();
				Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
				//Comentar antes da vers�o final
				ee.printStackTrace();
			}	
		}
	}