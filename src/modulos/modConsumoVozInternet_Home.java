package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.Home;
import paginas.Log;

public class modConsumoVozInternet_Home {
	////////////////////////////////////////////////////////////
	/////////////// Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;
	Util Ut = new Util(driver);
	long pageLoadTime_Seconds;
	Home HomePage;
	
	public void Teste_Home(WebDriver driver) throws IOException, AWTException, InterruptedException {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////
				
		Date myDateIni = null;
		StopWatch pageLoad = new StopWatch();
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		int count = 0;
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////	

		try {
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada
			///////////////////////////////////////////Data modifica��o 17/04/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			//Abre Site
			Log.info("Site MEUTIM - Inicio Jornada Home");	

			Ut.checkPageIsReady(driver,15);
			//boolean telaOk = driver.getPageSource().contains("PLANO");

			//if (telaOk == true )	{

				// Valida��o Consumo de dados e voz
				HomePage = new Home(driver);
				Ut.TempoLoad_Total();

				if (Ut.EsperaElemento(HomePage.tabela_consumo_dados) == false) {				
					pageLoad.stop();
					//Ut.MovePara(HomePage.lnk_DebAut, driver);
					Ut.FotoZ("Erro_Home_TabelaConsumoDados");
					Log.error("Erro na Home: Erro Tabela Consumo Dados.");
					//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Erro;Erro ao carregar Pagina Home;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Erro;Erro ao carregar Pagina Home;" + (int) Ut.TempoLoad_Total());
					driver.quit();
					Assert.assertTrue("Problema carregar tabela Consumo Dados", Ut.EsperaElemento(HomePage.tabela_consumo_dados));
				}
				
				Ut.MovePara(HomePage.tabela_consumo_dados, driver);
				Ut.FotoZ("TabelaConsumoDados");
				
				Log.info("VALIDANDO INFORMACOES DA FRANQUIA DE DADOS:");

				String[] dados = HomePage.secao_consumo.getText().split("\n");
				
				for (String info : dados){
					if (info.contains("|") || info.equals("")) {
						continue;
					}
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;"+ info +";Sucesso;NA;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
					Log.info("Texto:" + info);
				}
				
				//String card_home_stg = HomePage.card_consumo_dados.toString();
				//System.out.println("PRINTANDO O CARRRDDD" + card_home_stg);
				Ut.ValidarElementosNoHome(".//*[@id='V_b1c782d143b12b39fac018f6100000f7successBoxConsInfo']/div[3]", myDateIni);
				
				
				
//				if (dados[0].contains("Sistema temporariamente indispon?vel"))
//					log("Erro_Home_SecaoConsumoDados","Mensagem de erro: " + dados[0],dt, myDateIni, hr, false);
//				if (dados[0].replace("Franquia de dados ", "").replace("(", "").replace(")", "").replace("/", "").isEmpty())
//					log("Erro_Home_SecaoConsumoDados","O valor da Franquia de dados e a quantidade usada nao esta aparecendo: " + dados[0],dt, myDateIni, hr, false);
//				if (!dados[1].equals("Consumo detalhado"))
//					log("Erro_Home_SecaoConsumoDados","O Texto 'Consumo detalhado' nao esta aparecendo: " + dados[1],dt, myDateIni, hr, false);
//				if (!dados[2].contains("%") || dados[2].replace("%", "").isEmpty())
//					log("Erro_Home_SecaoConsumoDados","A porcentagem de uso nao esta aparecendo: " + dados[2],dt, myDateIni, hr, false);
//				if (!dados[5].contains("GB") || dados[5].replace("GB", "").isEmpty())
//					log("Erro_Home_SecaoConsumoDados","A quantidade usada de GB nao esta aparecendo: " + dados[5],dt, myDateIni, hr, false);
//				if (!dados[8].contains("GB") || dados[8].replace("GB", "").isEmpty())
//					log("Erro_Home_SecaoConsumoDados","A quantidade total de GB nao esta aparecendo: " + dados[8],dt, myDateIni, hr, false);

//				dados = HomePage.secao_dados.getText().split("\n");
//				
//				for (String info : dados){
//					if (info.contains("|") || info.equals("")) {
//						continue;
//					}
//					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;"+ info +";Sucesso;NA;" + (int) Ut.TempoLoad_Total());
//					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
//					Log.info("Texto:" + info);
//				}
//				
//				if (!dados[1].contains("GB") || dados[1].replace("GB", "").isEmpty() )
//					log("Erro_Home_SecaoConsumoDados","O Valor da franquia de dados esta vazia: " + dados[1],dt, myDateIni, hr, false);
//				if (!dados[2].contains("Franquia") || dados[2].replace("Franquia ", "").replace("GB", "").isEmpty() )
//					log("Erro_Home_SecaoConsumoDados","O Valor da franquia de dados esta vazia: " + dados[2],dt, myDateIni, hr, false);
//				if (!dados[3].contains("Pr�xima franquia:") || dados[3].replace("Pr�xima franquia: ", "").isEmpty() )
//					log("Erro_Home_SecaoConsumoDados","Data da Proxima Franquia vazia: " + dados[3], dt, myDateIni, hr, false);
//				
				Log.info("VALIDANDO INFORMACOES DO CONSUMO DE VOZ:");
				
				Ut.MovePara(HomePage.meu_consumo_voz, driver);
				Ut.FotoZ("TabelaConsumoVoz");
				dados = HomePage.meu_consumo_voz.getText().split("\n");
				
				for (String info : dados){
					if (info.contains("|") || info.equals("")) {
						continue;
					}
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;"+ info +";Sucesso;NA;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
					Log.info("Texto:" + info);
				}
				if (dados[0].contains("Sistema temporariamente indispon?vel"))
					log("Erro_Home_SecaoConsumoDados","Mensagem de erro: " + dados[0],dt, myDateIni, hr, false);
				if (!dados[1].contains("Minutos usados da franquia") || dados[1].replace("Minutos usados da franquia ", "").replace("(", "").replace(")", "").replace("/", "").replace("min", "").isEmpty() )
					log("Erro_Home_SecaoConsumoVoz","Informacoes de Dados de voz vazia.", dt, myDateIni, hr, false);
				if (!dados[2].contains("%") || dados[2].replace("%", "").isEmpty() )
					log("Erro_Home_SecaoConsumoVoz","Informacoes de Dados de voz vazia.", dt, myDateIni, hr, false);
				if (!dados[5].contains("min") || dados[5].replace("min", "").isEmpty() )
					log("Erro_Home_SecaoConsumoVoz","Informacoes de Dados de voz vazia.", dt, myDateIni, hr, false);
				if (!dados[8].contains("min") || dados[8].replace("min", "").isEmpty() )
					log("Erro_Home_SecaoConsumoVoz","Informacoes de Dados de voz vazia.", dt, myDateIni, hr, false);
				if (!dados[9].contains("Consumo referente ao ciclo de") || dados[9].replace("Consumo referente ao ciclo de ", "").isEmpty() )
					log("Erro_Home_SecaoConsumoVoz","Informacoes de Dados de voz vazia.", dt, myDateIni, hr, false);
				if (!dados[10].contains("Informa��es atualizadas at�") || dados[10].replace("Informa��es atualizadas at� ", "").isEmpty() )
					log("Erro_Home_SecaoConsumoVoz","Informacoes de Dados de voz vazia.", dt, myDateIni, hr, false);
				pageLoad.stop();
				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				Log.info("Site MEUTIM - Fim Jornada Home");	
				
			/*} else {
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.FotoZ("Erro_Home");
					Log.error("Erro na Home: pagina n�o carregada.");
					Log.endTestCase("Site_MeuTIM_HOME_ConsumoDadosVoz");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					Assert.assertTrue("Problema Consumo dados e voz", telaOk==true);
				}*/	
		
		}
	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
		///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
		////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			catch (Exception ee) {
				String ErMgs = "Erro";
				StringWriter sw = new StringWriter();
				ee.printStackTrace(new PrintWriter(sw));
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Home");
				Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
				Log.error(sw.toString());
				Log.endTestCase("Site_MeuTIM_HOME_ConsumoDadosVoz");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados e Voz;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				//driver.quit();
				Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
				//Comentar antes da vers�o final
				ee.printStackTrace();
			}	
		}
	
	public void log(String nomePrint, String erro, SimpleDateFormat dt, Date myDateIni, SimpleDateFormat hr, boolean result) throws IOException, InterruptedException, AWTException{
		Ut.FotoZ(nomePrint); //"Erro_Home_SecaoConsumoDados"
		Log.error("Erro na Home: " + erro); //"Erro na Home: Informacoes de Franquia Dados vazia."
		Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Erro;" + erro + ";" + (int) Ut.TempoLoad_Total());
		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
		+ ";Site MEU TIM - Jornada Conta Online;Home;Consumo Dados;NA;Erro;"  + erro + ";" + (int) Ut.TempoLoad_Total());
		driver.quit();
		Assert.assertTrue(erro, result);
	}
}