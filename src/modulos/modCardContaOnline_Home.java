package modulos;

import java.awt.AWTException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.StopWatch;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import config.Util;
import paginas.DownloadPdf;
import paginas.Home;
import paginas.Log;

public class modCardContaOnline_Home {
	////////////////////////////////////////////////////////////
	/////////////// Definir Variaveis Locais Classe///////////////////////////
	WebDriver driver;
	Util Ut = new Util(driver);
	long pageLoadTime_Seconds;
	DownloadPdf Download;
	Home HomePage;
	String downloadFilepath = "C:/Downloads/";  
	String dirDest = "C:/SitesTIM_Robo_ContaOnline/Downloads/";	
	boolean ResultadoDownload;
	
	public void Teste_Home(WebDriver driver) throws IOException, AWTException, InterruptedException {
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////
	
				
		Date myDateIni = null;
		String ArquivoPDF;		
		StopWatch pageLoad = new StopWatch();
		SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dt_pdf = new SimpleDateFormat("dd/MM/yy");
		long pageLoadTime_Seconds;
		////////////////////////////////////////////////////////////
		///////////////Definir Variaveis Locais Metodo///////////////////////////	

		try {
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
			///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
			////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
			///////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			myDateIni = new Date();
			pageLoad.reset();
			pageLoad.start();
			//Abre Site
			Log.info("Site MEUTIM - Inicio Jornada Home");	

			Ut.checkPageIsReady(driver,10);
			boolean telaOk = driver.getPageSource().contains("PLANO");

			if (telaOk == true )	{

				// Valida��o Card Minha Conta
				HomePage = new Home(driver);
				Ut.TempoLoad_Total();

				if (Ut.EsperaElemento(HomePage.Valor) == false) {				
					pageLoad.stop();
					Ut.MovePara(HomePage.lnk_DebAut, driver);
					Ut.FotoZ("Erro_Home_CardMinhaConta");
					Log.error("Erro na Home: Erro Card Minha Conta.");
					//Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home;" + (int) Ut.TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home;" + (int) Ut.TempoLoad_Total());
					driver.quit();
					Assert.assertTrue("Problema Card Minha Conta", Ut.EsperaElemento(HomePage.Valor));
				}	
				pageLoad.stop();
				Ut.FotoZ("Home");
				Ut.MovePara(HomePage.lnk_MinhaConta, driver);
				Ut.FotoZ("Home2");
				Log.info("Home carregada.");
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Sucesso;NA;" + (int) Ut.TempoLoad_Total());
				Log.info("Site MEUTIM - Fim Jornada Login");
				Log.info("Site MEUTIM - Fim Jornada Home");	
			} else {
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.FotoZ("Erro_Home");
					Log.error("Erro na Home: pagina n�o carregada.");
					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) Ut.TempoLoad_Total());		
					Assert.assertTrue("Problema Card Minha Conta", telaOk==true);
				}	
//			
//			//Validacao Fatura
//			Log.info("Site MEUTIM - Inicio Jornada Valida PDF");	
//			Log.info("Seleciona atalho para download da ultima fatura do cliente");
//			pageLoad.reset();
//			pageLoad.start();
//			Ut.ValidarElementosNoHomeTabela(HomePage.tr_collection);
//			Log.info("Seleciona janela de download do PDF");
//			Ut.troca_aba(1, driver);
//			Ut.checkPageIsReady(driver,40);
//			Log.info("Janela Download PDF Carregada");
//			Download.Atualiza(driver);
//			//String Tela = driver.getPageSource();
//			//boolean Tx = Tela.contains("Seu arquivo est� pronto para Download!");
//			//boolean Tx2 = Tela.contains("Confirma a gera��o do arquivo?");
//			
//			//Seu arquivo est� pronto para Download!				
////			if (Tx2 == false) { //Tx == false &&
////				pageLoad.stop();
////				pageLoadTime_Seconds = pageLoad.getTime() / 1000;
////				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
////					+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura;" + (int) pageLoadTime_Seconds);
////				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
////					+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura;" + (int) pageLoadTime_Seconds);
////				Ut.FotoZ("Erro_DownloadPDF");
////				Log.error("Erro no DownLoad do PDF");
////				Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
//		//
////			} 
//			
//			if (Download.btn_Sim.isDisplayed() == true) {
//				Download.btn_Sim.click();
//			}
//				
////			if (Tx2 == true) {
////				Log.info("Confirma��o Download do Arquivo");
////				Download = new DownloadPdf(driver);
////				Download.btn_Sim.click();
////				//Thread.sleep(5000);
////			} else {
////				Log.info("Download Autom�tico");
////				//Thread.sleep(5000);	
////			}
//			
//			
//			Log.info("Foto Tela Download PDF Carregada");
//			Ut.FotoZ("DownloadPDF");
//			//driver.close();
//			//Ut.troca_aba(0, driver);
//			
//			//Valida ser arquivo esta no diret�rio
//			Log.info("Espera finaliza��o do download do PDF Carregada");
//			Ut.checkArqReady(driver, 400, dirDest, ".pdf");
//			ArquivoPDF = Ut.NomeArq_Ext(dirDest, ".pdf");
//			Log.info("Nome do Arquivo: " + ArquivoPDF);
//			Log.info("Foto Tela Download PDF Carregada");
//			Ut.FotoZ("DownloadPDF");
//			Log.info("Fecha Tela Download PDF Carregada");
//			//driver.close();
//			//Ut.troca_aba(0, driver);
//			
//			//Validar valores
//			boolean vVencimento = Ut.PegaPdf(dt_pdf.format(HomePage.DtVencimento.getText()),dirDest+ArquivoPDF);
//			boolean vValor = Ut.PegaPdf(HomePage.Valor.getText(),dirDest+ArquivoPDF);
//			
//			
//			
//			pageLoad.stop();
//			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
//			//System.out.println(dt_pdf.format(HomePage.DtVencimento.getText()) + Ut.PegaPdf(dt_pdf.format(HomePage.DtVencimento.getText()),dirDest+ArquivoPDF));
//			//System.out.println(HomePage.Valor.getText() + Ut.PegaPdf(HomePage.Valor.getText(),dirDest+ArquivoPDF));
//
//		if (vVencimento == true || vValor == true) {
//			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		} 	if (vVencimento == true) {				
//			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		} if (vValor == true) {				
//			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		}
//		if (vVencimento == false) {				
//			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		} if (vValor == false) {				
//			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//			+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
//		}			
//			Log.info("Site MEUTIM - Fim Jornada Valida PDF");
		}

		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////// Bloco de C�digo do M�dulo / Funcionalidade a ser validada- Thiago Cruz
		///////////////////////////////////////////Data modifica��o 17/03/2017///////////////////////////////
		////////////////////////////////Tentar gerar 1 Funcionalidade X Classe no Package modulos////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
			catch (Exception ee) {
				String ErMgs = "Erro";
				StringWriter sw = new StringWriter();
				ee.printStackTrace(new PrintWriter(sw));
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Home");
				Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
				Log.error(sw.toString());
				Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
				pageLoad.stop();
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro na pagina Home: Erro Gen�rico.;" + (int) Ut.TempoLoad_Total());
				//driver.quit();
				Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
				//Comentar antes da vers�o final
				ee.printStackTrace();
			}	
		}
	}