import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import config.Util;
import test.contaOnline_Home;

public class Run {
	static WebDriver driver;
	static Util Ut = new Util(driver);
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws IOException {
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { contaOnline_Home.class });
		testng.addListener(tla);
		testng.run();
		
		
		//String texto = "13/03/17 R$ 4,05";
		
		//System.out.println(HomePage.DtVencimento.getText() + " " + HomePage.Valor.getText().trim());				
		//System.out.println(Ut.PegaPdf("13/03/17","C:/SitesTIM_Robo_ContaOnline/Downloads/"+"fatura_3935703_000582484-AA_20170313.pdf"));
		//System.out.println(Ut.PegaPdf("R$ 4,05","C:/SitesTIM_Robo_ContaOnline/Downloads/"+"fatura_3935703_000582484-AA_20170313.pdf"));
		
		
		//System.out.println("Arquivo foi deletado? "+ Ut.DeleteArq("C:/SitesTIM_Robo_ContaOnline/Downloads/"+"fatura_3935703_000582484-AA_20170313.pdf"));
	}		
}
