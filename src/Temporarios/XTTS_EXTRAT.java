package Temporarios;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class XTTS_EXTRAT {
	
		
	   WebDriver driver;
	   String baseUrl;
	   boolean acceptNextAlert = true;
	   StringBuffer verificationErrors = new StringBuffer();
	
	
	  

@Test (dataProvider="dp")	  
public void f(Integer n, String s) throws InterruptedException {
  
	driver.get(baseUrl);
	Thread.sleep(100);
    // type | id=username-id | T3217037
    driver.findElement(By.id("username-id")).clear();
    driver.findElement(By.id("username-id")).sendKeys("T3217037");// T3217037 T3471026
    // type | id=pwd-id | Thunder@1
    driver.findElement(By.id("pwd-id")).clear();
    driver.findElement(By.id("pwd-id")).sendKeys("Thunder@2"); // Thunder@2 everis@2017
    // click | id=submitBtn | 
    driver.findElement(By.id("submitBtn")).click();
    Thread.sleep(100);
    closeAlertAndGetItsText(driver, 10);
    driver.findElement(By.xpath("//a//div[text() ='Painel Principal de TI']")).click();
    closeAlertAndGetItsText(driver, 10);
    troca_aba(1,driver);
    checkPageIsReady(driver, 30);
    closeAlertAndGetItsText(driver, 10);
    ////PesquisaIN
    driver.findElement(By.linkText("Campos do Sistema")).click();
    driver.findElement(By.cssSelector("#WIN_0_1009 > div.btntextdiv > div.f7")).click();
    driver.findElement(By.id("arid_WIN_0_536871151")).clear();
    driver.findElement(By.id("arid_WIN_0_536871151")).sendKeys(s);//IN17131261
    driver.findElement(By.cssSelector("#WIN_0_1002 > div.btntextdiv > div.f7")).click();
    String StatusIN = driver.findElement(By.id("arid_WIN_0_7")).getText();
    // ERROR: Caught exception [unknown command []]
    driver.findElement(By.linkText("Data/Hora de...")).click();
    String DataCriaIN = driver.findElement(By.id("arid_WIN_0_3")).getText();
    String DataResolvidoIN = driver.findElement(By.id("arid_WIN_0_910000000")).getText();
    driver.findElement(By.linkText("Trabalho")).click();
    driver.findElement(By.xpath("//img[@alt='Editor para Di�rio de Trabalho/Solu��o, vazio']")).click();
    String TrabalhoHIst = driver.findElement(By.id("hEditor")).getText();
    driver.findElement(By.id("ardivcl")).click();
    driver.findElement(By.xpath("//img[@alt='Editor para Notas de Problema, vazio']")).click();
    String NotaFechamentoIN = driver.findElement(By.id("hEditor")).getText();
    driver.findElement(By.id("ardivcl")).click();
    System.out.println(StatusIN + ";" + DataCriaIN + ";" + DataResolvidoIN + ";" + TrabalhoHIst + ";" + NotaFechamentoIN);
    
    ///PesquisaIN
    
    closeAlertAndGetItsText(driver, 10);
    troca_aba(2,driver);
    checkPageIsReady(driver, 30);
    
    ///PesquisaPB
    driver.findElement(By.linkText("Problema Assoc")).click();
    // ERROR: Caught exception [ERROR: Unsupported command [waitForPopUp | 1491401943708 | 30000]]
    // ERROR: Caught exception [ERROR: Unsupported command [selectWindow | name=1491401943708 | ]]
    String PBNUM = driver.findElement(By.id("arid_WIN_0_536871151")).getText();
    String PBSTATUS = driver.findElement(By.id("arid_WIN_0_7")).getText();
    String NOTAPB = driver.findElement(By.id("arid_WIN_0_536870951")).getText();
    driver.findElement(By.linkText("Trabalho")).click();
    driver.findElement(By.xpath("//img[@alt='Editor para Di�rio de Trabalho/Solu��o, vazio']")).click();
    String HISTPB = driver.findElement(By.id("hEditor")).getText();
    driver.findElement(By.id("ardivcl")).click();
    String SDNPB = driver.findElement(By.id("arid_WIN_0_536871111")).getText();
    driver.findElement(By.linkText("Data/Hora de...")).click();
    String DTPB = driver.findElement(By.id("arid_WIN_0_3")).getText();
    String DTPBFECHAMENTO = driver.findElement(By.id("arid_WIN_0_910000000")).getText();
    driver.findElement(By.cssSelector("#WIN_0_800005799 > div.btntextdiv > div.f7")).click();
    System.out.println(PBNUM + ";" + PBSTATUS + ";" + NOTAPB + ";" + HISTPB + ";" + SDNPB + ";" + DTPB  + ";" + DTPBFECHAMENTO);
    ///PesquisaPB
    
    closeAlertAndGetItsText(driver, 10);
    ///Logout
    driver.get(" https://xtts.internal.timbrasil.com.br/arsys/forms/xtts/TBR_Painel+TI+New/Painel+TI/?cacheid=88d95c42&format=html");
    driver.findElement(By.xpath("//a[@id='WIN_0_800005799']/div/div")).click();
    
    	
    	
}

  @DataProvider
  public Object[][] dp() {
    return new Object[][] {
      new Object[] { 1, "IN17131261" },
      //new Object[] { 2, "b" },
    };
  }
  @BeforeClass
  public void setUp() throws Exception {
	  System.setProperty("webdriver.chrome.driver", "C:/Users/tpaivacr/workspace/XTT_A/driver/chromedriver.exe"); 
	  System.setProperty("webdriver.chrome.logfile", "C:/Users/tpaivacr/workspace/XTT_A/driver/chromedriver.log");
	  DesiredCapabilities capabilities = DesiredCapabilities.chrome(); 
	  capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
	  capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));
	  //ativa log
	  LoggingPreferences logPrefs = new LoggingPreferences();
	  logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
	  capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
	  
	  //Log performance
	  Map<String, Object> perfLogPrefs = new HashMap<String, Object>();
	  perfLogPrefs.put("traceCategories", "browser,devtools.timeline,devtools"); // comma-separated trace categories
	  ChromeOptions options = new ChromeOptions();
	  options.setExperimentalOption("perfLoggingPrefs", perfLogPrefs);
	  
	  driver = new ChromeDriver(capabilities); 
	  
	baseUrl = "https://xtts.internal.timbrasil.com.br/arsys/shared/login.jsp";
	driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	 driver.manage().deleteAllCookies();
	  }

  public void troca_aba(int valor, WebDriver driver){
	  ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles()); 
	  if (!availableWindows.isEmpty()) { 
	  driver.switchTo().window(availableWindows.get(valor)); 
	  
	  }	 
	
  }

  private void closeAlertAndGetItsText(WebDriver driver, int T) throws InterruptedException {
	    try {
	    
	    	
	    Alert alert = driver.switchTo().alert();
	     WebElement bmc = driver.findElement(By.xpath("//*[@id='PopupMsgFooter']/a"));
	    
	  
	    
	    for (int i=0; i<T; i++){   
	        Thread.sleep(1000);
	   	     // if (acceptNextAlert) {
	      
	        if (bmc.isDisplayed()){	        	
	        	bmc.click();
	        }
	        
	        alert.accept();
	     // } else {
	        //alert.dismiss();
	    //  }
	     // return alertText;
	    }
	    }catch (Exception e) {
	        e.printStackTrace();
	        }
  }
  
  public void checkPageIsReady(WebDriver driver,int T) {
	  
	  JavascriptExecutor js = (JavascriptExecutor)driver;
	  
	  
	  //Initially bellow given if condition will check ready state of page.
	  if (js.executeScript("return document.readyState").toString().equals("complete")){ 
	  //System.out.println("Page Is loaded.");
	   return; 
	  } 
	  
	  //This loop will rotate for 25 times to check If page Is ready after every 1 second.
	  //You can replace your value with 25 If you wants to Increase or decrease wait time.
	  for (int i=0; i<T; i++){ 
	   try {
	    Thread.sleep(1000);
	    }catch (InterruptedException e) {} 
	   //To check page ready state.
	   if (js.executeScript("return document.readyState").toString().equals("complete")){ 
	    break; 
	   }   
	  }
	 }
  @AfterClass
  public void afterClass() {
  }

  
  @BeforeTest
  public void beforeTest() {
  }

  
  
  @AfterTest
  public void afterTest() {
  }

}
