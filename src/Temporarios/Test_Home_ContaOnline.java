package Temporarios;

import static org.junit.Assert.assertTrue;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.xml.DOMConfigurator;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import config.Util;
import paginas.DownloadPdf;
import paginas.Home;
import paginas.Log;
import paginas.Login;

public class Test_Home_ContaOnline {

	static WebDriver driver;
	Login LoginPage;
	Home HomePage;
	DownloadPdf Download;
	Util Ut = new Util(driver);
	Date myDateIni;
	Date myDateFim;
	private String parsedText;
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
    boolean ResultadoDownload;
	 String downloadFilepath = "C:/Downloads/";  
	 String dirDest = "C:/SitesTIM_Robo_ContaOnline/Downloads/";
	 String ArquivoPDF;
	// Provide Log4j configuration settings

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		// Provide Log4j configuration settings
		DOMConfigurator.configure("log4j.xml");
		// DOMConfigurator.configure("C:/Users/tpaivacr/workspace/MeuTIM_SITE_V1.1/log4j.xml");
		Log.startTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
		System.setProperty("webdriver.chrome.driver",
				"driver/chromedriver.exe");
		// System.setProperty("webdriver.chrome.driver",
		// "C:/Users/tpaivacr/workspace/XTT_A/driver/chromedriver.exe");
		
		
		String downloadFilepath = "C:\\Downloads\\";
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		//WebDriver driver = new ChromeDriver(cap);
		
		driver = new ChromeDriver(cap);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		driver.get("https://meutim.tim.com.br/");
	}

	@Test
	public void Teste_Login() throws IOException, AWTException, InterruptedException {
		// Executa cen�rio
		try {

			// Realizada Login Site MeuTIM
			LoginPage = new Login(driver);
			HomePage = new Home(driver);
			myDateIni = new Date();
			LoginPage.LogIn_Action("21981178990", "8457");
			System.out.println("Data execu��o;Hora;KPI;Tela;Nome Card;Elemento;Carregou;Tempo Execu��o");
			
	
			// Valida��es na tela HOME
			// Valida LightBox da HOME - Site MeuTIM
			// HomePage.LightBox_NaTela();
			// driver.findElement(By.xpath("/html/body/div[7]"))
			//Log.info("Espera 15 segundos pela exibicao do elemento LightBox");
			//EsperaElemento(HomePage.div_LightBox);
	
			assertTrue(HomePage.LogIn_Sucesso());
			myDateFim = new Date();
		
			Log.info("Tira Foto de sucesso da Tela Home");
			Ut.Foto("Home", driver);			
			Log.info("Login Realizado com Sucesso");
			
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site - MeuTIM;Home;Login;NA;Sucesso;" + CalcData(myDateFim, myDateIni));
			
			// Novo Inicio
			myDateIni = new Date();

			// Valida MinhaConta
			Log.info("Bota em evidencia o elemento Card Minha Conta na tela");
			
			
			Ut.MovePara(HomePage.card_MinhaConta, driver);
			Log.info("Tira Foto de sucesso da Tela Home - Card Minha Conta");
			Ut.Foto("Card_MinhaConta", driver);			
			assertTrue(HomePage.Valor.isDisplayed());
			Log.info("Valor da Ultima Fatura exibido com sucesso");
			// assertFalse(HomePage.barra_Carregando.isDisplayed());
			// Data Fim
			myDateFim = new Date();
			Log.info("Pega data e hora fim da jornada do cliente ate o momento do carregamento do Card Minha Conta");
			//System.out.println("Data Vencimento Site: " + HomePage.DtVencimento.getText());
			//System.out.println("Valor Fatura Site: " + HomePage.Valor.getText());
			// System.out.println("Tempo de Execucao em Segundos: " +
			// CalcData(myDateFim, myDateIni) + " Data Fim: " + myDateFim + "
			// Data Inicio : " + myDateIni);
			Log.info("Tempo de Execucao em Segundos Ate Card Minha Conta: " + CalcData(myDateFim, myDateIni)
					+ " Data Fim: " + myDateFim + " Data Inicio : " + myDateIni);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site - MeuTIM;Home;Card MinhaConta;Lista Ultimas Faturas;Sucesso;" + CalcData(myDateFim, myDateIni));
			// Novo Inicio
			myDateIni = new Date();
			
			// Realiza Download PDF
			 Ut.ValidarElementosNoHomeTabela(HomePage.tr_collection);

			// Falta Corrigir problema do download do conta online para
			// implementar funcionalidade
			 Ut.troca_aba(0,driver);
			 
			 //Faz Download PDF
				myDateIni = new Date();
				HomePage.AtualizarZ();
				Ut.ValidarElementosNoHomeTabela(HomePage.tr_collection);
						
				Ut.troca_aba(1, driver);
				Download = new DownloadPdf(driver);
				String Tela = driver.getPageSource();//.contains("Escolha o perfil:");
				boolean Tx = Tela.contains("Seu arquivo est� pronto para Download!");
				boolean Tx2 = Tela.contains("Escolha o perfil:");
				
				//Seu arquivo est� pronto para Download!
				
				if (Tx == false && Tx2 == false) {
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni) + ";Site Conta Online;Home;Card Minha Conta;Download PDF;false;Erro Download;"
					 + Ut.CalcData(new Date(), myDateIni));
					Ut.Foto("Erro_DownloadPDF", driver);
					Log.error("Erro no DownLoad do PDF");
					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			
				} if (Tx2 == true) {
					Download.btn_Sim.click();
					Thread.sleep(10000);
					//System.out.println(downloadFilepath + " " + Ut.isFileDownloaded_Ext(downloadFilepath, ".pdf"));
					ResultadoDownload = Ut.isFileDownloaded_Ext(downloadFilepath, ".pdf");
				} else { 
					Thread.sleep(10000);			
					//System.out.println(downloadFilepath + " " + Ut.isFileDownloaded_Ext(downloadFilepath, ".pdf"));
					ResultadoDownload = Ut.isFileDownloaded_Ext(downloadFilepath, ".pdf");
				}
				
				//System.out.println(downloadFilepath + " " +  ResultadoDownload);
				
				if (ResultadoDownload == false) {		
					//String Er = driver.findElement(By.xpath("//*[contains(text(), 'N�o � poss�vel acessar esse site')]")).getText();
					Ut.Foto("Tela_Erro_Fatrura_",driver);
					Log.error("Erro PDF nao encontrado");
					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			
				} else
					 {
					ArquivoPDF = "21981527343_"+"_Fatura_"+HomePage.DtVencimento.getText();
						Ut.Foto("MinhaConta_DownladFatura_OK_",driver);
						System.out.println(ArquivoPDF + downloadFilepath + dirDest);						
						Ut.RenArqFat(ArquivoPDF,downloadFilepath,dirDest);
						System.out.println("Download Realizado com Sucesso" +" " +  dirDest+ArquivoPDF);
						
						
					}		
						
				Ut.troca_aba(0,driver);
				
				
				System.out.println(PegaPdf(HomePage.DtVencimento.getText(),dirDest+ArquivoPDF));
				System.out.println(PegaPdf(HomePage.Valor.getText(),dirDest+ArquivoPDF));
				
				Log.info("Site MEUTIM - Fim Jornada Valida PDF");
			 //Faz Download PDF

			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site - MeuTIM;Home;Card MinhaConta - Ultimo Vencimento;" + HomePage.DtVencimento.getText() + ";Sucesso;" + CalcData(myDateFim, myDateIni));

			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site - MeuTIM;Home;Card MinhaConta - Ultimo Valor Fatura;" + HomePage.Valor.getText() + ";Sucesso;" + CalcData(myDateFim, myDateIni));
			myDateFim = new Date();
			
			
			myDateFim = new Date();
			Log.info("Logout Realizado com Sucesso");
			Log.info("Tempo de Execucao da jornada em Segundos: " + CalcData(myDateFim, myDateIni) + " Data Fim: "
					+ myDateFim + " Data Inicio : " + myDateIni);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site - MeuTIM;Home;Logout;NA;Sucesso;" + CalcData(myDateFim, myDateIni));
			
			Realiza_Logout_Home();
			

		} catch (NoSuchElementException ee) {
			// Trata Exception
			myDateFim = new Date();
			String Saida = ee.getCause().toString();
			String SA =  Ut.ProcuraTexto("*** Element info:",Saida);
			String Limpo = Ut.LimpaTexto("*** Element info:",SA);
			Ut.Foto("Erro_Home", driver);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
		
			Log.error("Elemento Nao encontrado: " + SA + " " + sw.toString());

			try {
				Log.info("Tempo de Execucao da jornada em Segundos: " + CalcData(myDateFim, myDateIni) + " Data Fim: "
						+ myDateFim + " Data Inicio : " + myDateIni);
			} catch (Exception e) {
				Log.info("Data Fim: " + myDateFim + " Data Inicio : " + myDateIni);
				Log.error(e.getStackTrace().toString());
			}
			
			Log.error(ee.getStackTrace().toString());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni) + ";Site - MeuTIM;Home;NA;" + Limpo
					+ ";Erro;" + CalcData(myDateFim, myDateIni));
			ee.printStackTrace(pw);
		} catch (TimeoutException te) {
			// Trata Exception
			myDateFim = new Date();
			Ut.Foto("Erro_Home", driver);
			// System.out.println(te.getMessage() + " outra parte ");
			String Saida = te.getMessage();
			String SA = Ut.ProcuraTexto("xpath:",Saida);
			String Limpo = Ut.LimpaTexto("xpath:",Saida);

			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
		
			Log.error("Elemento Nao encontrado: " + SA + " " + sw.toString());

			try {
				Log.info("Tempo de Execucao da jornada em Segundos: " + CalcData(myDateFim, myDateIni) + " Data Fim: "
						+ myDateFim + " Data Inicio : " + myDateIni);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.info("Data Fim: " + myDateFim + " Data Inicio : " + myDateIni);
				Log.error(e.getStackTrace().toString());
			}

			Log.error(te.getStackTrace().toString());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni) + ";Site - MeuTIM;Home;NA;" + Limpo
					+ ";Erro;" + CalcData(myDateFim, myDateIni));
			
			te.printStackTrace(pw);
		} catch (Exception ee2) {
			// Trata Exception
			myDateFim = new Date();
			// System.out.println(te.getMessage() + " outra parte ");
			String Saida = ee2.getMessage();
			Ut.Foto("Erro_Home", driver);
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			
			Log.error("Erro generico Validar " + Saida + " " + sw.toString());

			try {
				Log.info("Tempo de Execucao da jornada em Segundos: " + CalcData(myDateFim, myDateIni) + " Data Fim: "
						+ myDateFim + " Data Inicio : " + myDateIni);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.info("Data Fim: " + myDateFim + " Data Inicio : " + myDateIni);
				Log.error(e.getStackTrace().toString());
			}

			Log.error(ee2.getStackTrace().toString());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni) + ";Site - MeuTIM;Home;NA;" + Saida
					+ ";Erro;" + CalcData(myDateFim, myDateIni));
			ee2.printStackTrace(pw);
		}
	}

	@After
	public void tearDown() throws Exception {

		driver.close();
		//quit();
	}

	public void EsperaElemento(WebElement We) throws AWTException {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.visibilityOf(We));
		Ut.cTeclado();
		// Ut.fecha();
	}

	public boolean PegaPdf(String reqTextInPDF, String arqPDF) throws InvalidPasswordException, IOException {
		boolean flag = false;
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		parsedText = null;
		PDFParser parser;

		try {
			File file = new File(arqPDF);
			parser = new PDFParser(new RandomAccessFile(file, "r"));
			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);
			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
			// System.out.println(parsedText);
		} catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed " + e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		if (parsedText.contains(reqTextInPDF)) {
			flag = true;
		}

		try {
			if (cosDoc != null)
				cosDoc.close();
			if (pdDoc != null)
				pdDoc.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return flag;
	}

	public long CalcData(Date dataAte, Date dataDe) {
		long diff = dataAte.getTime() - dataDe.getTime();// as given
		long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
		return seconds;
}

	private static long[] getTime(LocalDateTime dataAte, LocalDateTime dataDe) {
		final int MINUTES_PER_HOUR = 60;
		final int SECONDS_PER_MINUTE = 60;
		final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;

		LocalDateTime today = LocalDateTime.of(dataDe.getYear(), dataDe.getMonthValue(), dataDe.getDayOfMonth(),
				dataAte.getHour(), dataAte.getMinute(), dataAte.getSecond());
		Duration duration = Duration.between(today, dataDe);

		long seconds = duration.getSeconds();

		long hours = seconds / SECONDS_PER_HOUR;
		long minutes = ((seconds % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE);
		long secs = (seconds % SECONDS_PER_MINUTE);

		return new long[] { hours, minutes, secs };
	}
	
	

	public void Realiza_Logout_Home() {
		assertTrue(HomePage.LogIn_Sucesso());
		assertTrue(HomePage.Btn_Logout.isDisplayed());
		Ut.MovePara(HomePage.btn_Sair, driver);
		HomePage.lnk_Logout.click();
		assertTrue(HomePage.btn_ConfirmaSair.isDisplayed());
		HomePage.btn_ConfirmaSair.click();
		Log.info("Logout Realizado com Sucesso");
		Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	}

}
