package Temporarios;

import static org.junit.Assert.*;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import config.Util;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;

import java.awt.AWTException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import paginas.DownloadPdf;
import paginas.Home;
import paginas.Log;
import paginas.Login;
import paginas.Login_Error;

public class TesteNovo {
	
	static WebDriver driver;
	Login LoginPage;
	Home HomePage;
	Login_Error LoginErrorPage;
	DownloadPdf Download;
	Util Ut = new Util(driver);
	Date myDateIni;
	Date myDateFim;
	private String parsedText;
	SimpleDateFormat hr = new SimpleDateFormat("hh:mm:ss");
	SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat dt_pdf = new SimpleDateFormat("dd/MM/yy");
    boolean ResultadoDownload;
	 String downloadFilepath = "C:/Downloads/";  
	 String dirDest = "C:/SitesTIM_Robo_ContaOnline/Downloads/";
	 String ArquivoPDF;
	 double loadTime;
	 JavascriptExecutor js = null;
	 Long navigationStart; 
	 Long responseStart ;
	 Long domComplete;
	 Long backendPerformance;
	 Long frontendPerformance;
	 Long loadEventEnd;	 
	 StopWatch pageLoad = new StopWatch();
	   long pageLoadTime_ms;
       long pageLoadTime_Seconds;
     boolean Er;
     boolean Er2;
	 String msgEr;
	 
	@BeforeClass
	public void setUp() throws Exception {
		// Provide Log4j configuration settings
		DOMConfigurator.configure("Config/log4j.xml");
		Log.startTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
		System.setProperty("webdriver.chrome.driver",
				"Driver/chromedriver.exe");
		String downloadFilepath = "C:\\SitesTIM_Robo_ContaOnline\\Downloads\\";
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", downloadFilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		}

	@AfterClass
	public void tearDown() throws Exception {
		driver.quit();
		//System.out.println(dirDest+ArquivoPDF);
		//Log.info("Deleta Arquivo PDF");
		//System.out.println("Arquivo foi deletado? "+ Ut.DeleteArq(dirDest+ArquivoPDF));
	}

	@Test 
	public void Login_Site() throws AWTException, IOException, InterruptedException {
		try {
			myDateIni = new Date();
			//Layout Saida
			System.out.println("Data execu��o;Hora;KPI;Tela;Nome Card;Elemento;Carregou;Motivo;Tempo Execu��o");
			
			//Abre Site
			Log.info("Site MEUTIM - Inicio Jornada Login");	
			driver.get("https://meutim.tim.com.br/");
			//System.out.println(TempoLoad());
			
			checkPageIsReady(driver,10);
					
			boolean telaOk = driver.getPageSource().contains("Escolha o perfil");
				
			
			
if (telaOk == true)	{
			// Realizada Login Site MeuTIM
			Ut.FotoZ("Login");
			LoginPage = new Login(driver);
			pageLoad.start();
			LoginPage.LogIn_Action("21981178990", "8457");
			TempoLoad_Parcial();
			
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			
			pageLoad.reset();
			pageLoad.start();
			
			Er = LoginPage.Login_Error_ForaAr();
			Er2 = LoginPage.Login_Invalido();
			
					// Gera output
					if (Er == false && Er2 == false) {
						Log.info("Login Aceito");
						Log.info("Site Conta Online;Login;NA;Sucesso;");
						Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Sucesso;NA;" + TempoLoad_Parcial());
						System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Sucesso;NA;" + TempoLoad_Parcial());
					} else {
						// Pega valor em caso de erro
						LoginErrorPage =  new Login_Error(driver);
						msgEr = LoginErrorPage.msg_Erro.getText();
						Log.info("Valida Erro no Login");
						// Foto Tela Login erro
						Ut.FotoZ("Erro_Login");
						Log.error("Erro no Login: " + msgEr);
						Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
						// Falha Teste em Caso de retorno false
						
						if (Er == true) {
							Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Site Fora do Ar;" + TempoLoad_Parcial());
							System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Site Fora do Ar;" + TempoLoad_Parcial());
						} else {					
							Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Login ou Senha inv�lidos;" + TempoLoad_Parcial());
							System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
							+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Login ou Senha inv�lidos;" + TempoLoad_Parcial());
						}
						Assert.assertFalse("Sistema Fora do Ar", driver.getPageSource().contains("Desculpe, o sistema est�"));
						Assert.assertFalse("Usu�rio ou senha inv�lida", driver.getPageSource().contains("Usu�rio ou senha inv�lida"));
					}		
					Log.info("Site MEUTIM - Fim Jornada Login");	
			}	 else {			
				// Foto Tela Login erro
				Ut.FotoZ("Erro_Login");
				Log.error("Erro no Login: Problema na rede - pagina n�o carregada.");
				Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
				pageLoad.stop();
				pageLoadTime_Seconds = pageLoad.getTime() / 1000;
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + TempoLoad_Parcial());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + TempoLoad_Parcial());
				driver.quit();
				Assert.assertTrue("Problema na Rede", telaOk);
					}
		} catch (TimeoutException e) {
			// Foto Tela Login erro
			String ErMgs = "TimeOut";
			Ut.FotoZ("Erro_Login");
			Log.error("Erro no Login: Problema na rede - Timeout. " + e.getCause());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + pageLoadTime_Seconds);
			driver.quit();
			Assert.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
		}
		catch (NoSuchElementException el) {
			// Foto Tela Login erro
			String ErMgs = "TimeOut";
			Ut.FotoZ("Erro_Login");
			Log.error("Erro no Login: Problema na rede - Elemento n�o carregado." + el.getCause());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - Elemento n�o carregado;" + TempoLoad_Parcial());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + TempoLoad_Parcial());
			driver.quit();
			Assert.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
		}	
		catch (Exception ee) {
			// Foto Tela Login erro
			String ErMgs = "TimeOut";
			Ut.FotoZ("Erro_Login");
			Log.error("Erro no Login: Erro Gen�rico." + ee.getCause());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
			+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - Elemento n�o carregado;" + TempoLoad_Parcial());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Login;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + TempoLoad_Parcial());
			driver.quit();
			Assert.assertFalse("TimeOut", ErMgs.contains("TimeOut"));
		}		
	}
	
	@Test (dependsOnMethods = { "Login_Site" })
	public void Teste_Home() throws IOException, AWTException, InterruptedException {
		try {
			pageLoad.reset();
			pageLoad.start();
					//Abre Site
					Log.info("Site MEUTIM - Inicio Jornada Home");	
													
					checkPageIsReady(driver,10);
					boolean telaOk = driver.getPageSource().contains("PLANO");
				
			if (telaOk == true )	{
			
				// Valida��o Card Minha Conta
				HomePage = new Home(driver);
				TempoLoad_Total();

				if (EsperaElemento(HomePage.Valor) == false) {				
					pageLoad.stop();
					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
					Ut.MovePara(HomePage.lnk_DebAut, driver);
					Ut.FotoZ("Erro_Home_CardMinhaConta");
					Log.error("Erro na Home: Erro Card Minha Conta.");
					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home;" + (int) TempoLoad_Total());
					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
						+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home;" + (int) TempoLoad_Total());
					driver.quit();
					Assert.assertTrue("Problema Card Minha Conta", EsperaElemento(HomePage.Valor));
				 }	
				pageLoad.stop();
				pageLoadTime_Seconds = pageLoad.getTime() / 1000;
				Ut.FotoZ("Home");
				Ut.MovePara(HomePage.lnk_MinhaConta, driver);
				Ut.FotoZ("Home2");
				Log.info("Home carregada.");
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Sucesso;NA;" + (int) TempoLoad_Total());
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Sucesso;NA;" + (int) TempoLoad_Total());
				Log.info("Site MEUTIM - Fim Jornada Login");
				Log.info("Site MEUTIM - Fim Jornada Home");	
				//Validacao Fatura
				Log.info("Site MEUTIM - Inicio Jornada Valida PDF");	
				Log.info("Seleciona atalho para download da ultima fatura do cliente");
				pageLoad.reset();
				pageLoad.start();
				Ut.ValidarElementosNoHomeTabela(HomePage.tr_collection);
				Log.info("Seleciona janela de download do PDF");
				Ut.troca_aba(1, driver);
				checkPageIsReady(driver,10);
				Log.info("Janela Download PDF Carregada");
				String Tela = driver.getPageSource();
				boolean Tx = Tela.contains("Seu arquivo est� pronto para Download!");
				boolean Tx2 = Tela.contains("Confirma a gera��o do arquivo?");
				
				//Seu arquivo est� pronto para Download!				
//				if (Tx2 == false) { //Tx == false &&
//					pageLoad.stop();
//					pageLoadTime_Seconds = pageLoad.getTime() / 1000;
//					Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//						+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura;" + (int) pageLoadTime_Seconds);
//					System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
//						+ ";Site MEU TIM - Jornada Conta Online;Home;Download Fatura;NA;Erro;Erro na Pagina de Download Fatura;" + (int) pageLoadTime_Seconds);
//					Ut.FotoZ("Erro_DownloadPDF");
//					Log.error("Erro no DownLoad do PDF");
//					Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
//			
//				} 
				
				if (Tx2 == true) {
					Log.info("Confirma��o Download do Arquivo");
					Download = new DownloadPdf(driver);
					Download.btn_Sim.click();
					//Thread.sleep(5000);
				} else {
					Log.info("Download Autom�tico");
					//Thread.sleep(5000);	
				}
				
				
				Log.info("Foto Tela Download PDF Carregada");
				//Ut.FotoZ("DownloadPDF");
				//driver.close();
				//Ut.troca_aba(0, driver);
				
				//Valida ser arquivo esta no diret�rio
				Log.info("Espera finaliza��o do download do PDF Carregada");
				Ut.checkArqReady(driver, 400, dirDest, ".pdf");
				ArquivoPDF = Ut.NomeArq_Ext(dirDest, ".pdf");
				Log.info("Nome do Arquivo: " + ArquivoPDF);
				Log.info("Foto Tela Download PDF Carregada");
				Ut.FotoZ("DownloadPDF");
				Log.info("Fecha Tela Download PDF Carregada");
				//driver.close();
				//Ut.troca_aba(0, driver);
				
				//Validar valores
				boolean vVencimento = Ut.PegaPdf(dt_pdf.format(HomePage.DtVencimento.getText()),dirDest+ArquivoPDF);
				boolean vValor = Ut.PegaPdf(HomePage.Valor.getText(),dirDest+ArquivoPDF);
				
				
				
				pageLoad.stop();
				pageLoadTime_Seconds = pageLoad.getTime() / 1000;
				//System.out.println(dt_pdf.format(HomePage.DtVencimento.getText()) + Ut.PegaPdf(dt_pdf.format(HomePage.DtVencimento.getText()),dirDest+ArquivoPDF));
				//System.out.println(HomePage.Valor.getText() + Ut.PegaPdf(HomePage.Valor.getText(),dirDest+ArquivoPDF));
			
			if (vVencimento == true || vValor == true) {
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
			} 	if (vVencimento == true) {				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
			} if (vValor == true) {				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Consistente Web X PDF;" + (int) pageLoadTime_Seconds);
			}
			if (vVencimento == false) {				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vVencimento  + ";Data Vencimento Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
			} if (vValor == false) {				
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor + ";Valor Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Valida Fatura;Data Vencimento;" + vValor  + ";Valor Inconsistente Web X PDF;" + (int) pageLoadTime_Seconds);
			}			
				Log.info("Site MEUTIM - Fim Jornada Valida PDF");
				
			} else {
				pageLoad.stop();
				pageLoadTime_Seconds = pageLoad.getTime() / 1000;
				Ut.FotoZ("Erro_Home");
				Log.error("Erro na Home: pagina n�o carregada.");
				Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
				Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) TempoLoad_Total());		
				System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
					+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Erro ao carregar Pagina Home - Verificar Rede;" + (int) TempoLoad_Total());		
				driver.quit();
				Assert.assertTrue("Problema Card Minha Conta", telaOk==true);
			}
		}  catch (TimeoutException e) {
			String ErMgs = "Erro";
			//Foto Tela Login erro
			Ut.FotoZ("Erro_Home");
			Log.error("Erro ao Carregar Pagina Home: Problema na rede - Timeout. " + e.getCause());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;NA;NA;Erro;Problema na rede - pagina n�o carregada;" + (int) TempoLoad_Total());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Problema na rede - pagina n�o carregada;" + (int) TempoLoad_Total());
			driver.quit();
			Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
		}
		catch (NoSuchElementException el) {
			String ErMgs = "Erro";
			// Foto Tela Login erro
			Ut.FotoZ("Erro_Home");
			Log.error("Erro ao Carregar Card Minha Conta: Elemento n�o carregado." + el.getCause());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Problema na rede - Elemento n�o carregado;" + (int) TempoLoad_Total());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Problema na rede - Elemento n�o carregado;" + (int) TempoLoad_Total());
			driver.quit();
			Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
		}	
		catch (Exception ee) {
			String ErMgs = "Erro";
			// Foto Tela Login erro
			Ut.FotoZ("Erro_Home");
			Log.error("Erro na pagina Home: Erro Gen�rico." + ee.getCause());
			Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Problema na rede - pagina n�o carregada;" + (int) TempoLoad_Total());
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Card Minha Conta;NA;Erro;Problema na rede - pagina n�o carregada;" + (int) TempoLoad_Total());
			driver.quit();
			Assert.assertFalse("TimeOut", ErMgs.contains("Erro"));
		}			
	}
	

	@Test (dependsOnMethods = { "Teste_Home" })
	public void Logout() throws IOException, InterruptedException, AWTException {
		pageLoad.reset();
		pageLoad.start();
		// Valida��o Logout
		HomePage = new Home(driver);
		
		Log.info("Site MEUTIM - Inicio Jornada Logout");	

		checkPageIsReady(driver,10);
				
		//if (domComplete == true) {
			Log.info("Move tela para elemento de logout");
			Ut.MovePara(HomePage.btn_Sair, driver);	
			Log.info("Chama a��o de logout");
			HomePage.LogOut_Action();
		
		try {
			Log.info("Espera tela de confirma��o de logout");
			EsperaElemento(HomePage.btn_ConfirmaSair);
			Log.info("Confirmar logout");
			Ut.FotoZ("Home_Logout");
			HomePage.btn_ConfirmaSair.click();
			Log.info("Logout Realizado com sucesso");
			pageLoad.stop();
			pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Sucesso;NA;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Sucesso;NA;" + pageLoadTime_Seconds);
			
		} catch (TimeoutException e) {
			// Foto Tela Login erro
						Ut.FotoZ("Erro_Home_logout");
						Log.error("Erro na pagina Home ao realizar logout: Erro ao esperar retorno do elemento." + e.getCause());
						Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
						pageLoad.stop();
						pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro ao esperar elemento carregar;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro ao esperar elemento carregar;" + pageLoadTime_Seconds);
		}
		catch (NoSuchElementException e1) {
			// Foto Tela Login erro
						Ut.FotoZ("Erro_Home_logout");
						Log.error("Erro na pagina Home ao realizar logout: Erro ao procurar elemento." + e1.getCause());
						Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
						pageLoad.stop();
						pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro ao procurar elemento;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro ao procurar elemento;" + pageLoadTime_Seconds);
		}
		catch (Exception e2) {
			// Foto Tela Login erro
						Ut.FotoZ("Erro_Home_logout");
						Log.error("Erro na pagina Home ao realizar logout: Erro gen�rico." + e2.getCause());
						Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
						pageLoad.stop();
						pageLoadTime_Seconds = pageLoad.getTime() / 1000;
			Ut.FileWriterLog(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro gen�rico;" + pageLoadTime_Seconds);
			System.out.println(dt.format(myDateIni) + ";" + hr.format(myDateIni)
				+ ";Site MEU TIM - Jornada Conta Online;Home;Logout;NA;Erro;Erro gen�rico;" + pageLoadTime_Seconds);
		}
		Log.info("Site MEUTIM - Fim Jornada Logout");
//}	
		
	}
	
	

	public boolean EsperaElemento(WebElement We) throws AWTException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.visibilityOf(We));
			return true;
		} catch (Exception e) {
			return false;
		}
}
	public boolean EsperaPagina(WebDriver driver) {
	        WebDriverWait wait = new WebDriverWait(driver, 30);
	       boolean R =  wait.until(new ExpectedCondition<Boolean>() {
	            public Boolean apply(WebDriver driver) {
	            	boolean Resultado = js.executeScript("return document.readyState").toString().equals("complete");
	            	   System.out.println(Resultado);
	            	return Resultado;
	            }
	        });
	       System.out.println(R);
	       return R;
	    }	
	
	public void checkPageIsReady(WebDriver driver, int T) {
		  
		  JavascriptExecutor js = (JavascriptExecutor)driver;
		  
		  
		  //Initially bellow given if condition will check ready state of page.
		  if (js.executeScript("return document.readyState").toString().equals("complete")){ 
		  //System.out.println("Page Is loaded.");
		   return; 
		  } 
		  
		  //This loop will rotate for 25 times to check If page Is ready after every 1 second.
		  //You can replace your value with 25 If you wants to Increase or decrease wait time.
		  for (int i=0; i<T; i++){ 
		   try {
		    Thread.sleep(1000);
		    }catch (InterruptedException e) {} 
		   //To check page ready state.
		   if (js.executeScript("return document.readyState").toString().equals("complete")){ 
		    break; 
		   }   
		  }
		 }
		

	public void EsperaTeste(){
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor)driver;		
			
		final String JS_SCROLL_DOWN =
			    "var callback = arguments[0], page = document.documentElement, height = page.scrollHeight; " +
			    "window.scrollTo(0, height); " +
			    "(function fn(){ " +
			    "   if(page.scrollHeight != height && document.readyState == 'complete') " +
			    "      return callback(); " +
			    "   setTimeout(fn, 5); " +
			    "})();";
			js.executeAsyncScript(JS_SCROLL_DOWN);	
}
	
	public double TempoLoad_Total(){
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    // time of the process of navigation and page load
	    double loadTime = (Double) js.executeScript(
	        "return (window.performance.timing.loadEventEnd  - window.performance.timing.navigationStart) / 1000");
		return loadTime; //unloadEventEnd             
	}
	
	public double TempoLoad_Parcial(){
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    // time of the process of navigation and page load
	    double loadTime = (Double) js.executeScript(
	        "return (window.performance.timing.connectStart  - window.performance.timing.navigationStart) / 1000");
		return loadTime; //unloadEventEnd             
	}
	
}
