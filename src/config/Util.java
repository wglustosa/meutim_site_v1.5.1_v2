package config;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.ui.Select;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Util {

	static WebDriver driver;
	private File[] files;
	static String Evid = "C:/SitesTIM_Robo_ContaOnline/Evidencias/";
	static File scrFile;
	private boolean acceptNextAlert = true;
	public static WebDriverWait wait = null;
	private String fatPDF;

	// Construtor
	public Util(WebDriver driver) {
		Util.driver = driver;
	}

	public void ValidarElementosNoHome(String Valor, Date myDateIni) {

		int count = 0;
		// Seleciona um componente de arcordo com o xpath definido e jogo em um
		// objeto do tipo WebElement
		WebElement e = driver.findElement(By.xpath(Valor));
		//WebElement e = Valor;
		// Busca todos os elementos de um determinado tipo, pela className
		// dentro do objeto WebElement carregado anteriormente e joga em uma
		// lista
		//List<WebElement> downloadButtons = e.findElements(By.xpath(Valor));
		List<WebElement> downloadButtons = e.findElements(By.xpath(Valor));
		Date myDateFim = new Date();
		int Nelementos = downloadButtons.size();
		System.out.println("QUANTIDADE ELEMENTOS = " + downloadButtons.size());
		for (int i = 0; i < downloadButtons.size(); i++) {
			System.out.println(downloadButtons.get(i).isDisplayed() + "  " + downloadButtons.get(i).getText()); // downloadButtons.get(i).getAttribute("alt")
																												// +
																												// "
																												// "
																												// +
		}
		System.out.println(myDateIni + " " + myDateFim + " Elementos: " + Nelementos);
	}

	private List<WebElement> findElements(By xpath) {
		// TODO Auto-generated method stub
		return null;
	}

	public void ValidarElementosNoHomeTabela(List<WebElement> tr_collection) {

		int count = 0;
		// Seleciona um componente de arcordo com o xpath definido e jogo em um
		// objeto do tipo WebElement

		int Nelementos = tr_collection.size();

		int row_num, col_num;
		row_num = 1;

		for (WebElement trElement : tr_collection) {

			List<WebElement> td_collection = trElement.findElements(By.xpath("td"));
			// System.out.println("NUMBER OF COLUMNS="+td_collection.size());
			col_num = 1;

			for (WebElement tdElement : td_collection) {
				if (row_num == 2 && col_num == 4) {

					tdElement.click();
				}
				col_num++;
			}
			row_num++;

		}

	}

	public void abrePdf(String pdf) throws IOException, InterruptedException {
		File file = new File(pdf);
		Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + file);
		Thread.sleep(5000);
	}

	public long CalcData(Date dataAte, Date dataDe) {
		long diff = dataAte.getTime() - dataDe.getTime();// as given
		long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
		return seconds;
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	public void fecha() throws AWTException {

		if (isElementPresent(By.xpath("/html/body/div[7]")) == true) {
			cTeclado();
		}
	}

	public void cTeclado() throws AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public void trocaTela() throws AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_TAB);
	}

	public void fechaTela() throws AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_F4);
	}

	public void MovePara(WebElement Xp, WebDriver driver) {
		Actions actions = new Actions(driver);
		actions.moveToElement(Xp).perform();
	}

	public void clicar(WebElement Xp, WebDriver driver) {
		Actions actions = new Actions(driver);
		actions.moveToElement(Xp, 10, 10).click().build().perform();
	}

	public String ProcuraTexto(String searchString, String inputString) {
		int Fim;
		int Ini;
		StringBuilder builder = new StringBuilder(inputString);

		// System.out.println("The capacity of the String " +
		// builder.capacity());

		// System.out.println("pos of" + builder.indexOf(searchString));

		// System.out.println(searchString);
		if (builder.length() > 0 && builder.length() <= 100) {
			Fim = 100;
		} else {
			Fim = builder.length();
			Ini = builder.indexOf(searchString);
			return builder.substring(Ini, Fim);
		}
		if (builder.length() <= 0) {
			return "Sem retorno";
		} else {
			Ini = builder.indexOf(searchString);
			return builder.substring(Ini, Fim);
		}
	}

	public String LimpaTexto(String searchString, String inputString) {
		int Fim;
		int Ini;
		try {
			StringBuilder builder = new StringBuilder(inputString);
			// System.out.println("The capacity of the String " +
			// builder.capacity());
			// System.out.println("pos of" + builder.indexOf(searchString));
			// System.out.println(searchString);
			// int Fim = builder.length();
			if (builder.length() > 0 || builder.length() <= 100) {
				Fim = 100;
			} else {
				Fim = builder.length();
				Ini = 0;
				return builder.substring(Ini, Fim);
			}
			if (builder.length() <= 0) {
				return "Sem retorno";
			} else {
				Ini = 0;
				return builder.substring(Ini, Fim);
			}
		} catch (Exception e) {
			return "Sem retorno";

		}
	}

	public void RenArqFat(String Nome, String dirPath, String dirDest) throws IOException {
		File dir = new File(dirPath);
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy_hhmmss");
		String DataArq = dt.format(new Date());
		String Arquivo = dirDest + Nome + "_" + DataArq + ".pdf";

		files = dir.listFiles();
		if (files != null || files.length != 0) {
			for (int i = 0; i < files.length; i++) {
				// System.out.println(files[i].getName() + " " + Arquivo );
				if (files[i].getName().contains("pdf")) {
					// Renomeia Arquivo PDF encontrado
					// System.out.println(files[i].getName() + " " + Arquivo);
					FileUtils.moveFile(FileUtils.getFile(dirPath + files[i].getName()), FileUtils.getFile(Arquivo));
				}
			}
		}
	}

	/* Valida ser um arquivo de determinada extens�o existe em um diret�rio */
	public boolean isFileDownloaded_Ext(String dirPath, String ext) {
		boolean flag = false;
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			flag = false;
		}
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().endsWith(ext)) {
				flag = true;
			}
		}
		return flag;
	}

	public String NomeArq_Ext(String dirPath, String ext) {
		String Arq = "";
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			Arq = "";
		}
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().contains(ext)) {
				Arq = files[i].getName();
				setFatPDF(Arq);
			}
		}
		return Arq;
	}

	public boolean DeleteArq(String Arq) {
		// boolean Resultado = false;
		File arquivo = new File(Arq);
		// Path path = FileSystems.getDefault().getPath(Arq);
		try {
			// Filesdelete(path);
			FileUtils.forceDelete(arquivo);
			return true;
		} catch (IOException | SecurityException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	public void checkArqReady(WebDriver driver, int T, String dirPath, String ext) {

		boolean Achou = isFileDownloaded_Ext(dirPath, ext);

		// Initially bellow given if condition will check ready state of page.
		if (Achou == true) {
			// System.out.println("Page Is loaded.");
			return;
		}

		// This loop will rotate for 25 times to check If page Is ready after
		// every 1 second.
		// You can replace your value with 25 If you wants to Increase or
		// decrease wait time.
		for (int i = 0; i < T; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			// To check page ready state.
			Achou = isFileDownloaded_Ext(dirPath, ext);
			if (Achou == true) {
				break;
			}
		}
	}

	public void troca_aba(int valor, WebDriver driver) {
		ArrayList<String> availableWindows = new ArrayList<String>(driver.getWindowHandles());
		if (!availableWindows.isEmpty()) {
			driver.switchTo().window(availableWindows.get(valor));

		}

	}

	private void click(String elemento) {
		driver.findElement(By.xpath(elemento)).click();
	}

	private void Escreve(String elemento, String valor, WebDriver driver) {
		driver.findElement(By.xpath(elemento)).sendKeys(valor);
		;
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private boolean Espera(String Xp, WebDriver driver) {
		boolean resultado = false;

		resultado = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(Xp))).isDisplayed();

		return resultado;

	}

	public void Foto(String NomeTela, WebDriver driver) throws IOException, InterruptedException {
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy_hhmmss");
		String DataArq = dt.format(new Date());
		String Arquivo = Evid + NomeTela + "_" + DataArq + ".jpg";
		// System.out.println(Arquivo);
		Thread.sleep(1000);
		scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(Arquivo));
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}

	public String FileWriterLog(String escrita) {

		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		Date date = new Date();

		// String Para data atual
		String dateFormated = dateFormat.format(date);

		// Para instanciar um objeto do tipo File:
		File arquivo = new File("C:/SitesTIM_Robo_ContaOnline/Dashboard/" + dateFormated + ".txt");
		try {
			// verifica se o arquivo ou diret�rio existe
			boolean existe = arquivo.exists();

			// caso seja um diret�rio, � poss�vel listar seus arquivos e
			// diret�rios
			File[] arquivos = arquivo.listFiles();

			// construtor que recebe tamb�m como argumento se o conte�do ser�
			// acrescentado
			// ao inv�s de ser substitu�do (append)
			FileWriter fwm = new FileWriter(arquivo, true);

			// escreve o conte�do no arquivo
			fwm.write(escrita + "\r\n");

			// fecha os recursos
			fwm.close();
			// fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return escrita;
	}

	public boolean PegaPdf(String reqTextInPDF, String arqPDF) throws InvalidPasswordException, IOException {
		boolean flag = false;
		PDFTextStripper pdfStripper = null;
		PDDocument pdDoc = null;
		COSDocument cosDoc = null;
		Object parsedText = null;
		PDFParser parser;

		try {
			File file = new File(arqPDF);
			parser = new PDFParser(new RandomAccessFile(file, "r"));
			parser.parse();
			cosDoc = parser.getDocument();
			pdfStripper = new PDFTextStripper();
			pdfStripper.setStartPage(1);
			pdfStripper.setEndPage(1);
			pdDoc = new PDDocument(cosDoc);
			parsedText = pdfStripper.getText(pdDoc);
			// System.out.println(parsedText);
		} catch (MalformedURLException e2) {
			System.err.println("URL string could not be parsed " + e2.getMessage());
		} catch (IOException e) {
			System.err.println("Unable to open PDF Parser. " + e.getMessage());
			try {
				if (cosDoc != null)
					cosDoc.close();
				if (pdDoc != null)
					pdDoc.close();
			} catch (Exception e1) {
				e.printStackTrace();
			}
		}
		if (((String) parsedText).contains(reqTextInPDF)) {
			// System.out.println((String) parsedText);
			flag = true;
		}

		try {
			if (cosDoc != null)
				cosDoc.close();
			if (pdDoc != null)
				pdDoc.close();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		return flag;
	}

	public void FotoZ(String NomeTela) throws IOException, InterruptedException, AWTException {
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy_hhmmss");
		String DataArq = dt.format(new Date());
		String Arquivo = Evid + NomeTela + "_" + DataArq + ".jpg";
		Thread.sleep(1000);
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension d = tk.getScreenSize();
		Rectangle rec = new Rectangle(0, 0, d.width, d.height);
		Robot ro = new Robot();
		BufferedImage img = ro.createScreenCapture(rec);
		File f = new File(Arquivo);// set appropriate path
		ImageIO.write(img, "jpg", f);
	}

	public void FotoSelenium(String NomeTela) throws IOException, InterruptedException, AWTException {

		// String activateDateTime = Keys.chord(Keys.CONTROL, Keys.ARROW_DOWN);
		// new
		// Actions(driver).keyDown(Keys.ALT).sendKeys(String.valueOf('\u0061')).keyUp(Keys.ALT).perform();
		new Actions(driver).sendKeys(String.valueOf('\u0001')).perform();

		// new Actions(driver).sendKeys("Q");
		SimpleDateFormat dt = new SimpleDateFormat("ddMMyyyy_hhmmss");
		String DataArq = dt.format(new Date());
		String Arquivo = Evid + NomeTela + "_" + DataArq + ".jpg";
		Thread.sleep(1000);
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(Arquivo));
	}

	public String getFatPDF() {
		return fatPDF;
	}

	public void setFatPDF(String fatPDF) {
		this.fatPDF = fatPDF;
	}

	public boolean EsperaElemento(WebElement We) throws AWTException {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.visibilityOf(We));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean EsperaPagina(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		JavascriptExecutor js = null;
		boolean R = wait.until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				boolean Resultado = js.executeScript("return document.readyState").toString().equals("complete");
				System.out.println(Resultado);
				return Resultado;
			}
		});
		System.out.println(R);
		return R;
	}

	public void checkPageIsReady(WebDriver driver, int T) {

		JavascriptExecutor js = (JavascriptExecutor) driver;

		// Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString().equals("complete")) {
			// System.out.println("Page Is loaded.");
			return;
		}

		// This loop will rotate for 25 times to check If page Is ready after
		// every 1 second.
		// You can replace your value with 25 If you wants to Increase or
		// decrease wait time.
		for (int i = 0; i < T; i++) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
			// To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete")) {
				break;
			}
		}
	}
	
public void TrocaPagina(String urlAntiga,WebDriver driver, int T){
		
		String urlNova = driver.getCurrentUrl();
		  //Initially bellow given if condition will check ready state of page.
		  if (!urlAntiga.equals(urlNova)){ 
//		  System.out.println("Printando URL ANTIGA"+ urlAntiga);
//		  System.out.println("Printando URL NOVA"+ urlNova);
		   return; 
		   
		  } 
		  
		  //This loop will rotate for 25 times to check If page Is ready after every 1 second.
		  //You can replace your value with 25 If you wants to Increase or decrease wait time.
		  for (int i=0; i<T; i++){ 
		   try {
		    Thread.sleep(1000);
		    }catch (InterruptedException e) {} 
		   //To check page ready state.
		   //System.out.println("Printando no LOOP");
		   if (!urlAntiga.equals(urlNova)){ 
		    break; 
		   }   
		  }
		
		
	}

	public void EsperaTeste() {
		driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
		JavascriptExecutor js = (JavascriptExecutor) driver;

		final String JS_SCROLL_DOWN = "var callback = arguments[0], page = document.documentElement, height = page.scrollHeight; "
				+ "window.scrollTo(0, height); " + "(function fn(){ "
				+ "   if(page.scrollHeight != height && document.readyState == 'complete') "
				+ "      return callback(); " + "   setTimeout(fn, 5); " + "})();";
		js.executeAsyncScript(JS_SCROLL_DOWN);
	}

	public double TempoLoad_Total() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		// time of the process of navigation and page load
		double loadTime = (Double) js.executeScript(
				"return (window.performance.timing.loadEventEnd  - window.performance.timing.navigationStart) / 1000");
		return loadTime; // unloadEventEnd
	}

	public double TempoLoad_Parcial() {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		// time of the process of navigation and page load
		double loadTime = (Double) js.executeScript(
				"return (window.performance.timing.connectStart  - window.performance.timing.navigationStart) / 1000");
		return loadTime; // unloadEventEnd
	}

	public void TrocarFrame(WebElement elemento) {
		driver.switchTo().frame(elemento);
	}

	public void TrocarParentFrame() {
		driver.switchTo().parentFrame();
	}

	public void WaitElementVisible(WebElement elemento) {
		for (int i = 0; i <= 10; i++) {
			try {
				if (elemento.isDisplayed())
					return;
			} catch (Exception ex) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ex.printStackTrace();
				continue;
			}
		}
	}
	
	public void Select(WebElement elemento, String value) {
		for (int i = 0; i <= 10; i++) {
			try {
				 new Select(elemento).selectByVisibleText(value);
			} catch (Exception ex) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ex.printStackTrace();
				continue;
			}
		}
	}

}
