package config;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ConfigAmb2 {
	 //private static ConfigAmb instance=null;
	 static WebDriver driver;
	

	//Construtor
	 ConfigAmb2(){ //WebDriver driver
       // ConfigAmb.driver = driver;
    }
	
//	public WebDriver setUpChrome() throws Exception {
//		// Provide Log4j configuration settings
//				System.setProperty("webdriver.chrome.driver",
//						"Driver/chromedriver.exe");
//				String downloadFilepath = "C:\\SitesTIM_Robo_ContaOnline\\Downloads\\";
//				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
//				chromePrefs.put("profile.default_content_settings.popups", 0);
//				chromePrefs.put("download.default_directory", downloadFilepath);
//				ChromeOptions options = new ChromeOptions();
//				options.setExperimentalOption("prefs", chromePrefs);
//				DesiredCapabilities cap = DesiredCapabilities.chrome();
//				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//				cap.setCapability(ChromeOptions.CAPABILITY, options);
//				driver = new ChromeDriver(cap);
//				driver.manage().window().maximize();
//				driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
//				return driver;
//		}
	
//	 public static ConfigAmb getInstance(){
//	        if(instance==null){
//	            instance = new ConfigAmb();
//	        }
//	        return instance;
//	    }
	 
	   //Configurar outros browsers - Futura Melhoria
	   public static WebDriver getWebDriverInstance(String No) {
	       // if (null == driver) {
	        	// Provide Log4j configuration settings
				//System.setProperty("webdriver.chrome.driver",
				//		"Driver/chromedriver.exe");
				String downloadFilepath = "C:\\SitesTIM_Robo_ContaOnline\\Downloads\\";
				HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
				chromePrefs.put("profile.default_content_settings.popups", 0);
				chromePrefs.put("download.default_directory", downloadFilepath);
				ChromeOptions options = new ChromeOptions();
				options.setExperimentalOption("prefs", chromePrefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				//driver = new ChromeDriver(cap);
				try {
					driver = new RemoteWebDriver(new URL(No),cap);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	      //  }
	        return driver;
	    }

	    public static void closeWebBrowser(){
	        driver.quit();
	        driver = null;
	}
	
}
