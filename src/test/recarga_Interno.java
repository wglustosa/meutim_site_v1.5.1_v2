package test;

import org.testng.annotations.Test;
import config.ConfigAmb;
import modulos.modRecarga_Infinity;
import modulos.modRecarga_Interno;
import paginas.Log;

import org.testng.annotations.BeforeClass;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class recarga_Interno {
	WebDriver driver = ConfigAmb.getWebDriverInstance();
	ConfigAmb ca;
	modRecarga_Interno mConsumo = new modRecarga_Interno ();

  //Gerar um novo @test para cada m�dulo -> Gerar uma classe de test para cada cen�rio de teste 	
  @Test
  public void Teste_LoadPage() throws Exception {
	 driver.get("http://www.tim.com.br");
  }
  
  @Test (dependsOnMethods = { "Teste_LoadPage" })
  public void Teste_Recarga() throws Exception {
	  mConsumo.Teste_Home(driver);
  }
  
  //Gerar um novo @test para cada m�dulo -> Gerar uma classe de test para cada cen�rio de teste 	
  
  @BeforeClass
  public void Config_Browser() throws Exception {
	   DOMConfigurator.configure("Config/log4j.xml");
	   Log.startTestCase("Site_TIM_HOME_Recarga_Infinity");
	   	
  }

  @AfterClass
  public void afterClass() {
	   Log.endTestCase("Site_TIM_HOME_Recarga_Infinity");
	   ConfigAmb.closeWebBrowser();
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
