package test;

import org.testng.annotations.Test;

import config.ConfigAmb;
import config.ConfigAmb2;
import modulos.modAlertasPortasAbertas_Mapa;
import modulos.modCardContaOnline_Home;
import modulos.modConsumoVozInternet_Home;
import modulos.modLogin;
import modulos.modLogout;
import modulos.modMapaCobertura_Mapa;
import modulos.modRecarga_Home;
import modulos.modSaldo_Home;
import modulos.modValidaFatura_Home;
import paginas.Log;

import org.testng.annotations.BeforeClass;

import java.util.HashMap;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class alertasDoPortasAbertas_TIM {
	WebDriver driver = ConfigAmb.getWebDriverInstance();
	ConfigAmb ca;
	modAlertasPortasAbertas_Mapa mConsumo = new modAlertasPortasAbertas_Mapa();

  //Gerar um novo @test para cada m�dulo -> Gerar uma classe de test para cada cen�rio de teste 	
  @Test
  public void Teste_LoadPage() throws Exception {
	 driver.get("http://www.tim.com.br");
  }
  
  @Test (dependsOnMethods = { "Teste_LoadPage" })
  public void Teste_Recarga() throws Exception {
	  mConsumo.Teste_Home(driver);
  }
  
  //Gerar um novo @test para cada m�dulo -> Gerar uma classe de test para cada cen�rio de teste 	
  
  @BeforeClass
  public void Config_Browser() throws Exception {
	   DOMConfigurator.configure("Config/log4j.xml");
	   Log.startTestCase("Site_TIM_HOME_Recarga");
	   	
  }

  @AfterClass
  public void afterClass() {
	   Log.endTestCase("Site_TIM_HOME_Recarga");
	   ConfigAmb.closeWebBrowser();
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
