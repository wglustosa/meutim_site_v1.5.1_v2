package test;

import org.testng.annotations.Test;

import config.ConfigAmb;
import config.ConfigAmb2;
import modulos.modCardContaOnline_Home;
import modulos.modLogin;
import modulos.modLogout;
import modulos.modValidaFatura_Home;
import paginas.Log;

import org.testng.annotations.BeforeClass;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

public class contaOnline_Home {
	WebDriver driver = ConfigAmb.getWebDriverInstance();
	ConfigAmb ca;
	modLogin mIn = new modLogin();
	modLogout mOut = new modLogout();
	modCardContaOnline_Home mCardOn = new modCardContaOnline_Home();
	modValidaFatura_Home mValFat = new modValidaFatura_Home();
	
  //Gerar um novo @test para cada m�dulo -> Gerar uma classe de test para cada cen�rio de teste 	
  @Test
  public void Teste_Login() throws Exception {
	mIn.Login(driver);
  }
  
  @Test (dependsOnMethods = { "Teste_Login" })
  public void Teste_CardContaOnline() throws Exception {
	  mCardOn.Teste_Home(driver);
  }
  
  @Test (dependsOnMethods = { "Teste_CardContaOnline" })
  public void Teste_PdfContaOnline() throws Exception {
	  mValFat.Teste_FuturaHome(driver);
  }
    
  @Test (dependsOnMethods = { "Teste_PdfContaOnline" })
  public void Teste_Logout() throws Exception {
	mOut.Logout(driver);
  }  
  
  //Gerar um novo @test para cada m�dulo -> Gerar uma classe de test para cada cen�rio de teste 	
  
  @BeforeClass
  public void Config_Browser() throws Exception {
	   DOMConfigurator.configure("Config/log4j.xml");
	   Log.startTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	   	
  }

  @AfterClass
  public void afterClass() {
	   Log.endTestCase("Site_MeuTIM_HOME_Card_MINHACONTA");
	   ConfigAmb.closeWebBrowser();
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

}
