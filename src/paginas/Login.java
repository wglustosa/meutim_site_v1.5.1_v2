package paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class Login {
	final WebDriver driver;
	
	//Elementos na Pagina
	@FindBy(how = How.XPATH, using = "//*[@id='seu_numero']")
	public WebElement input_MSISDN;

	@FindBy(how = How.XPATH, using = "//*[@id='senha']")
	public WebElement input_Pw;

	@FindBy(how = How.XPATH, using = "//button[text()[contains(.,'Entrar')]]")
	public WebElement btn_Login;
	
	
	//@FindBy(how = How.XPATH, using = "//*[@id='formLogin1']/div[9]/span")
	//public WebElement msg_Erro ;
	
 

	// This is a constructor, as every page need a base driver to find web elements
 	public Login(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		//This initElements method will create all WebElements
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,20);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);		
		}
    
 	
    //Metodos da Pagina
	public void LogIn_Action(String sUserName, String sPassword){
		 
		input_MSISDN.sendKeys(sUserName);
		Log.info("Utilizando MSISDN: " + sUserName);  
		input_Pw.sendKeys(sPassword);
		Log.info("Preenchendo Senha");  
		Log.info("Realizando um click no Botao Enviar");
		btn_Login.click();
	}

	public boolean Login_Error_ForaAr(){
		boolean Er = driver.getPageSource().contains("Desculpe, o sistema est�");
		return Er;	    
	}

	public boolean Login_Invalido(){
		boolean Er = driver.getPageSource().contains("Usu�rio ou senha inv�lida");
		return Er;	    
	}
	

}
    
    


