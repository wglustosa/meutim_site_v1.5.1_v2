package paginas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class Home {
	WebDriver driver;

	////*[@id="V_9b8db9f410f7915d2d048bc4100000f7tente"]   -- Card Minha Conta Link Atualizar
	
	@FindBy(how = How.XPATH, using = "//*[@id='minhaConta']/div[3]/ul/li[1]/a")
	public WebElement lnk_MinhaConta;	
	
	//Elementos na Pagina
	@FindBy(how = How.XPATH, using = "/html/body/div[7]")
	public WebElement div_LightBox;
	
	@FindBy(how = How.XPATH, using = "//*[@id='reloadPath']")
	public WebElement card_MinhaConta;
	
	@FindBy(how = How.XPATH, using = "//*[@id='reloadPath']/div[3]/table/tbody/tr")
	public List <WebElement> tr_collection;

	@FindBy(how = How.XPATH, using = "//*[@id='mainContent']/div[5]/div[1]/h2/a")
	public WebElement lnk_DebAut;
		
	@FindBy(how = How.XPATH, using = "//*[@id='reloadPath']/div[3]/table/tbody/tr[2]/td[2]")
	public WebElement text_ValorFatura;
	
	@FindBy(how = How.XPATH, using = "//*[@id='botao1']")
	public WebElement btn_UltimaFatura;
	
	@FindBy(how = How.XPATH, using = "//*[@id='container']/div[1]/div[2]/div/div[5]/a/img")
	public WebElement btn_Sair;
		
	@FindBy(how = How.XPATH, using = "//*[@id='submitBtn']")
	public WebElement btn_ConfirmaSair;
	
	//   //*[@id='linkLogout']/img 
	@FindBy(how = How.XPATH, using = "//*[@id='container']/div[1]/div[2]/div/div[5]/a")
	public WebElement Btn_Logout ;
	
	@FindBy(how = How.XPATH, using = "//*[@id='container']/div[1]/div[2]/div/div[5]/a")
	public WebElement lnk_Logout;
	
	@FindBy(how = How.XPATH, using = "//*[@id='reloadPath']/div[3]/table/tbody/tr[2]/td[1]/a")
	public WebElement DtVencimento;
	
	@FindBy(how = How.XPATH, using = "//*[@id='reloadPath']/div[3]/table/tbody/tr[2]/td[2]")
	public WebElement Valor;
	
	@FindBy(how = How.XPATH, using = "//*[@id='V_9b8db9f410f7915d2d048bc4100000f7loadingInvoiceInfo']/img")
	public WebElement barra_Carregando;
	
	@FindBy(how = How.XPATH, using = "//h3[text()[contains(.,'Meu Consumo de Dados')]]/parent::div/parent::div/div[3]")
	public WebElement tabela_consumo_dados;
	
	@FindBy(how = How.XPATH, using = ".//*[@id='V_b1c782d143b12b39fac018f6100000f7successBoxConsInfo']/div[3]")
	public WebElement card_consumo_dados;
	
	//*[@id="V_b1c782d143b12b39fac018f6100000f7successBoxConsInfo"]
	
	@FindBy(how = How.XPATH, using = "//div[@class='valor_recarga']/div")
	public WebElement tabela_saldo;
	
	@FindBy(how = How.XPATH, using = "//span[@class='valor_saldo ']")
	public WebElement header_saldo;
	
	@FindBy(how = How.XPATH, using = "//span[text()[contains(.,'MEUS CR�DITOS')]]")
	public WebElement menu_meusCreditos;
	
	@FindBy(how = How.XPATH, using = "//h3[text()[contains(.,'Meu Consumo de Dados')]]/parent::div/parent::div/div[3]/div[@class='dados_plano']")
	public WebElement secao_dados;
	
	@FindBy(how = How.XPATH, using = "//h3[text()[contains(.,'Meu Consumo de Dados')]]/parent::div/parent::div/div[3]/div[@class='consumo']")
	public WebElement secao_consumo;
	
	@FindBy(how = How.XPATH, using = "//h3[text()[contains(.,'Meu Consumo de Dados')]]/parent::div/parent::div/div[3]/div[@class='bloco']")
	public WebElement secao_dados_consumidos;
    
	@FindBy(how = How.XPATH, using = "//h3[text()[contains(.,'Meu Consumo de voz')]]/parent::div/parent::div/div[3]")
	public WebElement meu_consumo_voz;
	
	@FindBy(how = How.XPATH, using = "/html/body/div[@class='bg_modal']")
	public WebElement propaganda;
	
	// This is a constructor, as every page need a base driver to find web elements
 	public Home(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,15);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	
 	}
    
 	
    //Metodos da Pagina
	public boolean LogIn_Sucesso(){
		  boolean resultado = Btn_Logout.isDisplayed(); 
		  Log.info("Validando se login foi realizado com sucesso: Botao Logout exibido? " + resultado);  
		  return resultado;
	}        
	
	public void AtualizarZ() throws InterruptedException, AWTException{
		//Conta paga 
		boolean Er = driver.getPageSource().contains("Conta paga");
		if (Er == false){
			Robot robot = new Robot();
			  robot.keyPress(KeyEvent.VK_F5);
			  robot.keyRelease(KeyEvent.VK_F5);
			Thread.sleep(1000);				
		}
	}
	
	public void Atualiza(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		//This initElements method will create all WebElements
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,10);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	 		
	}
	
	public boolean LogOut_Action(){
		Log.info("Valida se Bot�o Logout est� visivel na tela");
		if (Btn_Logout.isDisplayed() == true){
			Log.info("Realiza um click no bot�o logout");
			lnk_Logout.click();
			return true;			
		} else
		{
			return false;
		}		 
		}	
}
