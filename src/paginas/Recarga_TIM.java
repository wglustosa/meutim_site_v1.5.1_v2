package paginas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class Recarga_TIM {
	WebDriver driver;

	@FindBy(how = How.XPATH, using = "//h4[text()[contains(.,'1. Escolha uma recarga ou pacote turbo')]]")
	public WebElement textoRecargaPagInterna;
	
	@FindBy(how = How.XPATH, using = "//div[@class='btn-group-recarga']")
	public WebElement grupoRecargaPagInterna;
	
	@FindBy(how = How.XPATH, using = "//input[@placeholder='Seu N�mero TIM']")
	public WebElement inputRecargaPagInterna;

	
	// This is a constructor, as every page need a base driver to find web elements
 	public Recarga_TIM(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,15);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	
 	}
}
