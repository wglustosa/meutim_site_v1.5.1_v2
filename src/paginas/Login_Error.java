package paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class Login_Error {
	final WebDriver driver;
		
	@FindBy(how = How.XPATH, using = "//*[@id='formLogin1']/div[9]/span")
	public WebElement msg_Erro;
		
	// This is a constructor, as every page need a base driver to find web elements
 	public Login_Error(WebDriver driver) { 	
 		this.driver = driver; 		
		//This initElements method will create all WebElements
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,10);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);		
		}
	
}
