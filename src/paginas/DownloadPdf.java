package paginas;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class DownloadPdf {
	WebDriver driver;

	//Elementos na Pagina 
	@FindBy(how = How.XPATH, using = "//*[@id='main-message']/h1")
	public WebElement lbn_Erro;
	
	@FindBy(how = How.XPATH, using = "//*[@id='botao1']")
	public WebElement btn_Sim;
	
	@FindBy(how = How.XPATH, using = "//*[@id='reloadPath']/div[3]/table/tbody/tr[2]/td[2]")
	public WebElement Valor;
		    
	// This is a constructor, as every page need a base driver to find web elements
 	public DownloadPdf(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		//This initElements method will create all WebElements
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,10);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	 		
 		}
    
 	public void Atualiza(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		//This initElements method will create all WebElements
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,10);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	 		
 		} 		
 	}
 	
 	
    //Metodos da Pagina
	/*public void LogIn_Action(String sUserName, String sPassword){
		 
		input_MSISDN.sendKeys(sUserName);
 
		input_Pw.sendKeys(sPassword);
 
		btn_Login.click();
 
	}*/

	//public boolean LogIn_Sucesso(){
		 // boolean resultado = Btn_Logout.isDisplayed(); 
	//	  return resultado;
	//}        
	
	

