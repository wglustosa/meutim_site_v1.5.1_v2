package paginas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class Home_TIM {
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//h1[text()[contains(.,'Recarga Express')]]")
	public WebElement recargaExpress;
	
	@FindBy(how = How.XPATH, using = "//*[@id='childPage']/iframe")
	public WebElement iframeRecargaExpress;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Planos. Conhe�a as Ofertas e Planos TIM..']")
	public WebElement menuPlanos;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Recarga. Recarga online para celular.']")
	public WebElement menuRecarga;
	
	@FindBy(how = How.XPATH, using = "/html/body/div[1]/div[2]/div[2]/div/div[8]/div[4]/ul/li[1]/div/div/div[1]/div[2]/ul/li[2]/a/span")//"//span[text()[contains(.,'Infinity Turbo 7')]]")
	public WebElement menuInfinityTurbo7;
	
	@FindBy(how = How.XPATH, using = "//label[text()[contains(.,'contrate agora pagando com o seu cart�o de cr�dito')]]")
	public WebElement labelContrateCartaoCredito;
	
	@FindBy(how = How.XPATH, using = "//input[@id='login_cliente_msisdn']")
	public WebElement inputContrateCartaoCredito;
	
	@FindBy(how = How.XPATH, using = "//iframe[@id='frame-turbo']")
	public WebElement iframeInfinityTurbo;
	
	@FindBy(how = How.XPATH, using = "//div[@class='tim-page-wrapper']/iframe")
	public WebElement iframeRecargaPagInterna;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Mapa de Cobertura. Confira a cobertura da TIM em todo o Brasil.']")
	public WebElement menuMapaCobertura;
	
	// This is a constructor, as every page need a base driver to find web elements
 	public Home_TIM(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,15);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	
 	}
}
