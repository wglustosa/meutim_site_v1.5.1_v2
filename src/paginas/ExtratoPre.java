package paginas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ExtratoPre {
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//p[text()[contains(.,'Consultar detalhamento de consumo')]]")
	public WebElement buttonConsultarDetalheConsumo;

	@FindBy(how = How.XPATH, using = "//select[@id='periodo']")
	public WebElement selectPeriodo;
	
	@FindBy(how = How.XPATH, using = "//input[@id='data_inicio']")
	public WebElement dataInicio;
	
	@FindBy(how = How.XPATH, using = "//input[@id='data_fim']")
	public WebElement dataFim;
	
	@FindBy(how = How.XPATH, using = "//input[@id='todosServicos']")
	public WebElement checkTodosServicos;
	
	@FindBy(how = How.XPATH, using = "//button[@title='Gerar Relatorio']")
	public WebElement buttonGerarRelatorio;

	@FindBy(how = How.XPATH, using = "//h1[text()[contains(.,'Detalhamento de Consumo')]]")
	public WebElement tituloDetalhamentoConsumo;
	
	@FindBy(how = How.XPATH, using = "//table[@class='tabela_consumo']")
	public WebElement tabelaDetalhamentoConsumo;

	@FindBy(how = How.XPATH, using = "//p[text()[contains(.,'N�o houve utiliza��o no per�odo consultado')]]")
	public WebElement msgSemConsumo;
	
	// This is a constructor, as every page need a base driver to find web elements
 	public ExtratoPre(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,15);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	
 	}
}
