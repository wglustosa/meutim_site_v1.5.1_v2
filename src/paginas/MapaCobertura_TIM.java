package paginas;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class MapaCobertura_TIM {
	WebDriver driver;
	
	@FindBy(how = How.XPATH, using = "//a[@title='Mapa de Cobertura. Confira a cobertura da TIM em todo o Brasil.']")
	public WebElement menuMapaCobertura;
	
	@FindBy(how = How.XPATH, using = "//input[@id='inputEntrada']")
	public WebElement inputCEP;
	
	@FindBy(how = How.XPATH, using = "//div[@id='childPage']/iframe")
	public WebElement iframeMapaCobertura;
	
	@FindBy(how = How.XPATH, using = "//div[@id='mapa-parent']/iframe")
	public WebElement iframeMapaCoberturaParent;
	
	@FindBy(how = How.XPATH, using = "//img[@id='inputEntradaLupa']")
	public WebElement buttonLupa;
	
	@FindBy(how = How.XPATH, using = "//div[@title='Mostrar mapa de ruas']")
	public WebElement buttonMapa;

	@FindBy(how = How.XPATH, using = "//div[@id='map_canvas']")
	public WebElement mapa;
	
	@FindBy(how = How.XPATH, using = "//div[@title='Mostrar imagens de sat�lite']")
	public WebElement buttonSatelite;
	
	@FindBy(how = How.XPATH, using = "//select[@id='uf']")
	public WebElement selectUF;
	
	@FindBy(how = How.XPATH, using = "//select[@id='ddd']")
	public WebElement selectDDD;
	
	@FindBy(how = How.XPATH, using = "//select[@id='cidade']")
	public WebElement selectCidade;
	
	@FindBy(how = How.XPATH, using = "//a[text()[contains(.,'CONSULTAR')]]")
	public WebElement buttonConsultar;
	
	@FindBy(how = How.XPATH, using = "//div[@class='fancybox-inner']/iframe")
	public WebElement iframeAlertasTim;
	
	@FindBy(how = How.XPATH, using = "//body")
	public WebElement popupAlertasTim;
	
	// This is a constructor, as every page need a base driver to find web elements
 	public MapaCobertura_TIM(WebDriver driver) 	{ 	
 		this.driver = driver; 		
		AjaxElementLocatorFactory fac =new AjaxElementLocatorFactory(driver,15);
        //PageFactory.initElements(driver, this);
 		PageFactory.initElements(fac, this);	
 	}
}
